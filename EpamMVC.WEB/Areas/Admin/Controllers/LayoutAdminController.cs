﻿using System.Web.Mvc;
using EpamMVC.DLL.Extensions;
using EpamMVC.DLL.Interfaces;
using EpamMVC.WEB.Areas.Admin.Models.Layout;
using Microsoft.AspNet.Identity;

namespace EpamMVC.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "Администраторы")]
    public class LayoutAdminController : Controller
    {
        private readonly IApplicationUserManager _userManager;

        public LayoutAdminController(IApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        [ChildActionOnly]
        public PartialViewResult Header()
        {
            return PartialView();
        }

        [ChildActionOnly]
        public PartialViewResult Footer()
        {
            return PartialView();
        }

        [ChildActionOnly]
        public PartialViewResult Sidebar()
        {
            return PartialView(
                new SidebarViewModel
                {
                    UserFullName = User.Identity.Name,
                    CurrentUser = _userManager.FindById(
                        User.Identity.GetUserId<int>()
                    ).ToApplicationUser()
                });
        }
    }
}
