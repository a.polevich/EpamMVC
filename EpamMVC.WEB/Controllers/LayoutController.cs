﻿using System.Web.Mvc;
using EpamMVC.WEB.Models.Layout;

namespace EpamMVC.WEB.Controllers
{
    public class LayoutController : Controller
    {
        [ChildActionOnly]
        public PartialViewResult Header()
        {
            return PartialView(
                new HeaderViewModel
                {
                    SiteName = "Ipsum Company",
                    LogoUrl = "/Content/Upload/logo.png"
                });
        }

        [ChildActionOnly]
        public PartialViewResult Footer()
        {
            return PartialView();
        }
    }
}
