﻿import $ from 'jquery';
import tinymce from 'tinymce/tinymce';

import 'tinymce/themes/silver';
import 'tinymce/skins/content/default/content.css';
import 'tinymce/skins/content/default/content.min.css';
import 'tinymce/skins/ui/oxide/skin.min.css';
import 'tinymce/skins/ui/oxide/content.min.css';
import 'tinymce/plugins/paste';
import 'tinymce/plugins/link';

$(function () {
    tinymce.init({
        selector: '.wysiwyg',
        plugins: ['paste', 'link'],
        base_url: "/Content/dist/tinymce"
    });
});
