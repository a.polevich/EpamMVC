﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Extensions;
using EpamMVC.DLL.Interfaces;
using EpamMVC.DLL.Models;
using EpamMVC.DLL.Models.Identity;
using EpamMVC.WEB.Models;
using EpamMVC.WEB.Models.Manage;
using EpamMVC.WEB.Utils;
using Microsoft.AspNet.Identity;

namespace EpamMVC.WEB.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess
        }

        private const int PageSize = 20;
        private IClientAccountService _clientAccountService;
        private IClientPaymentService _clientPaymentService;
        private IApplicationUserManager _userManager;

        public ManageController(
            IApplicationUserManager userManager,
            IClientAccountService clientAccountService,
            IClientPaymentService clientPaymentService
        )
        {
            _userManager = userManager;
            _clientAccountService = clientAccountService;
            _clientPaymentService = clientPaymentService;
        }

        private int CurrentUserId => User.Identity.GetUserId<int>();

        public async Task<ActionResult> Index(
            ManageMessageId? message,
            string sort = ""
        )
        {
            var sortVariants = new Dictionary<string, (string, string, OrderDirection)>
            {
                // Same as "по наименованию счета"
                {"byId", ("По номеру", "Id", OrderDirection.Descending)},
                {"byAmountAsc", ("По остатку", "Balance", OrderDirection.Descending)}
            };

            var sorterLinks = new Dictionary<string, string>();

            foreach (var variant in sortVariants)
                sorterLinks[variant.Value.Item1] = Url.Action("Index", new
                {
                    sort = variant.Key
                });

            if (!sortVariants.ContainsKey(sort)) sort = sortVariants.First().Key;

            var currentSort = sortVariants[sort];

            var clientAccounts = await Index_GetClientAccounts(
                currentSort.Item2,
                currentSort.Item3
            );

            ViewBag.PageMeta = new PageMetaContext
            {
                MetaTitle = "Личный кабинет",
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Личный кабинет"
                    }
                }
            };

            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Вы успешно сменили пароль."
                : message == ManageMessageId.SetPasswordSuccess ? "Вы успешно установили пароль."
                : "";

            return View(new IndexViewModel
            {
                User = await _userManager.FindByIdAsync(CurrentUserId),
                HasPassword = HasPassword(),
                ClientAccounts = clientAccounts,
                SorterLinks = sorterLinks,
                CurrentSortingLabel = currentSort.Item1
            });
        }

        public async Task<ActionResult> AccountDetails(
            int id,
            int pageIndex = 1,
            string sort = ""
        )
        {
            var account = await _clientAccountService.GetSingleAsync(
                x => x.UserId == CurrentUserId && x.Id == id
            );

            if (account == null) return HttpNotFound();

            var sortVariants =
                new Dictionary<string, (string, string, OrderDirection)>
                {
                    {"byId", ("По номеру", "Id", OrderDirection.Descending)},
                    {"byAmountAsc", ("По возрастанию суммы", "Amount", OrderDirection.Ascending)},
                    {"byAmountDesc", ("По убыванию суммы", "Amount", OrderDirection.Descending)},
                    {"byDateAsc", ("От старых к новым", "CreatedDateTime", OrderDirection.Ascending)},
                    {"byDateDesc", ("По новых к старым", "CreatedDateTime", OrderDirection.Descending)}
                };

            var sorterLinks = new Dictionary<string, string>();

            foreach (var variant in sortVariants)
                sorterLinks[variant.Value.Item1] = Url.Action("AccountDetails", new
                {
                    Id = id,
                    sort = variant.Key
                });

            if (!sortVariants.ContainsKey(sort)) sort = sortVariants.First().Key;

            var currentSort = sortVariants[sort];

            var payments = await AccountDetails_GetPayments(
                pageIndex,
                PageSize,
                currentSort.Item2,
                currentSort.Item3,
                account.Id
            );

            ViewBag.PageMeta = new PageMetaContext
            {
                MetaTitle = "Информация по карте " + account.Id,
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Личный кабинет"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("AccountDetails", new {id = account.Id}),
                        Text = "Информация по карте " + account.Id
                    }
                }
            };

            return View(new AccountDetailsViewModel
            {
                ClientAccount = account,
                AccountPayments = payments,

                Pagination = new Pagination
                {
                    UrlTemplate = Url.Action("AccountDetails", new
                    {
                        Id = id,
                        pageIndex = "PAGE_INDEX"
                    }),
                    PageSize = PageSize,
                    CurrentPageNumber = pageIndex,
                    TotalItems = payments.TotalCount
                },
                SorterLinks = sorterLinks,
                CurrentSortingLabel = currentSort.Item1
            });
        }


        private async Task<List<ClientAccount>> Index_GetClientAccounts(
            string property,
            OrderDirection order
        )
        {
            switch (property)
            {
                default:
                case "Id":
                    return await _clientAccountService.GetAllAsync(
                        x => x.Id,
                        order,
                        x => x.UserId == CurrentUserId
                    );
                case "Balance":
                    return await _clientAccountService.GetAllAsync(
                        x => x.Balance,
                        order,
                        x => x.UserId == CurrentUserId
                    );
            }
        }

        private async Task<PaginatedList<ClientPayment>> AccountDetails_GetPayments(
            int pageIndex,
            int pageSize,
            string property,
            OrderDirection order,
            int accountId
        )
        {
            switch (property)
            {
                default:
                case "Id":
                    return await _clientPaymentService.GetAsync(
                        pageIndex,
                        pageSize,
                        x => x.Id,
                        order,
                        x => x.AccountId == accountId
                    );
                case "Amount":
                    return await _clientPaymentService.GetAsync(
                        pageIndex,
                        pageSize,
                        x => x.Amount,
                        order,
                        x => x.AccountId == accountId
                    );
                case "CreatedDateTime":
                    return await _clientPaymentService.GetAsync(
                        pageIndex,
                        pageSize,
                        x => x.CreatedDateTime,
                        order,
                        x => x.AccountId == accountId
                    );
            }
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            var result = await _userManager.ChangePasswordAsync(CurrentUserId, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await _userManager.FindByIdAsync(CurrentUserId);
                if (user != null) await SignInAsync(user.ToAppUser(), false);
                return RedirectToAction("Index", new {Message = ManageMessageId.ChangePasswordSuccess});
            }

            AddErrors(result);
            return View(model);
        }

        //
        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _userManager.AddPasswordAsync(CurrentUserId, model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByIdAsync(CurrentUserId);
                    if (user != null) await SignInAsync(user.ToAppUser(), false);
                    return RedirectToAction("Index", new {Message = ManageMessageId.SetPasswordSuccess});
                }

                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager?.Dispose();
                _userManager = null;
                _clientAccountService?.Dispose();
                _clientAccountService = null;
                _clientPaymentService?.Dispose();
                _clientPaymentService = null;
            }

            base.Dispose(disposing);
        }

        private async Task SignInAsync(AppUser user, bool isPersistent)
        {
            _userManager.SignOut(_userManager.ExternalCookie, _userManager.TwoFactorCookie);
            _userManager.SignIn(isPersistent,
                await _userManager.CreateIdentityAsync(user, _userManager.ApplicationCookie));
        }

        private void AddErrors(ApplicationIdentityResult result)
        {
            foreach (var error in result.Errors) ModelState.AddModelError("", error);
        }

        private bool HasPassword()
        {
            var user = _userManager.FindById(CurrentUserId);
            return user?.PasswordHash != null;
        }

        [HttpPost]
        public async Task<ActionResult> AjaxCreateAccount()
        {
            var accountCount = await _clientAccountService.CountAsync(
                x => x.UserId == CurrentUserId
            );

            if (accountCount > 100) return HttpNotFound();

            await _clientAccountService.AddAsync(new ClientAccount
            {
                Locked = false,
                UserId = CurrentUserId,
                Balance = 0
            });

            return Json(new
            {
                result = "success"
            });
        }

        [HttpPost]
        public async Task<ActionResult> AjaxLockAccount(int accountId)
        {
            var account = await _clientAccountService.GetSingleAsync(
                x =>
                    x.UserId == CurrentUserId
                    && x.Id == accountId
                    && x.Locked == false
            );

            if (account == null) return HttpNotFound();

            account.Locked = true;

            await _clientAccountService.UpdateAsync(account);

            return Json(new
            {
                result = "success"
            });
        }

        [HttpPost]
        public async Task<ActionResult> AjaxUnlockAccount(int accountId)
        {
            var account = await _clientAccountService.GetSingleAsync(
                x =>
                    x.UserId == CurrentUserId
                    && x.Id == accountId
                    && x.Locked
                    && x.UnlockRequest == false
            );

            if (account == null) return HttpNotFound();

            account.UnlockRequest = true;

            await _clientAccountService.UpdateAsync(account);

            return Json(new
            {
                result = "success"
            });
        }

        public async Task<ActionResult> AjaxAddFunds(
            int accountId,
            decimal amount
        )
        {
            var account = await _clientAccountService.GetSingleAsync(
                x =>
                    x.UserId == CurrentUserId
                    && x.Id == accountId
                    && x.Locked == false
            );

            if (account == null) return HttpNotFound();

            account.Balance += amount;

            await _clientAccountService.UpdateAsync(account);

            return Json(new
            {
                result = "success"
            });
        }

        [HttpPost]
        public async Task<ActionResult> AjaxSendPayment(int paymentId)
        {
            var payment = await _clientPaymentService.GetSingleAsync(
                x => x.Id == paymentId
            );

            if (payment == null) return HttpNotFound();

            var account = await _clientAccountService.GetSingleAsync(
                x =>
                    x.UserId == CurrentUserId
                    && x.Id == payment.AccountId
                    && x.Locked == false
            );

            if (account == null) return HttpNotFound();

            payment.Status = ClientPayment.Statuses.Send;
            payment.SendDateTime = DateTime.Now;

            await _clientPaymentService.UpdateAsync(payment);

            return Json(new
            {
                result = "success"
            });
        }

        public async Task<ActionResult> PrintPayment(int id)
        {
            var payment = await _clientPaymentService.GetSingleAsync(
                x => x.Id == id
            );

            if (payment == null) return HttpNotFound();

            var account = await _clientAccountService.GetSingleAsync(
                x =>
                    x.UserId == CurrentUserId
                    && x.Id == payment.AccountId,
                x => x.User
            );

            if (account == null) return HttpNotFound();

            return View(
                new PrintPaymentViewModel
                {
                    ClientPayment = payment,
                    ClientAccount = account
                }
            );
        }

        public async Task<ActionResult> AjaxCreatePayment(int id, decimal amount)
        {
            var account = await _clientAccountService.GetSingleAsync(
                x =>
                    x.UserId == CurrentUserId
                    && x.Id == id
                    && x.Locked == false
            );

            if (account == null) return HttpNotFound();

            if (account.Balance < amount)
                return Json(new
                {
                    result = "not_enough"
                });

            account.Balance -= amount;
            await _clientAccountService.UpdateAsync(account);
            await _clientPaymentService.AddAsync(new ClientPayment
            {
                Amount = amount,
                CreatedDateTime = DateTime.Now,
                Status = ClientPayment.Statuses.Prepare,
                AccountId = account.Id
            });

            return Json(new
            {
                result = "success"
            });
        }
    }
}
