﻿using EpamMVC.DLL.Entities;

namespace EpamMVC.BLL.Interfaces
{
    public interface IClientAccountService : IService<ClientAccount>
    {
    }
}
