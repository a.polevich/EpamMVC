﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Extensions;
using EpamMVC.DLL.Interfaces;
using EpamMVC.DLL.Models.Identity;
using EpamMVC.WEB.Areas.Admin.Models.UserAdmin;
using EpamMVC.WEB.Models;
using EpamMVC.WEB.Utils;

namespace EpamMVC.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "Администраторы")]
    public class UsersAdminController : Controller
    {
        private const int PageSize = 20;
        private IClientAccountService _clientAccountService;
        private IApplicationRoleManager _roleManager;
        private IApplicationUserManager _userManager;

        public UsersAdminController(
            IApplicationUserManager userManager,
            IApplicationRoleManager roleManager,
            IClientAccountService clientAccountService
        )
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _clientAccountService = clientAccountService;
        }

        public async Task<ActionResult> Index(int pageIndex = 1)
        {
            ViewBag.PageMeta = new PageMetaContext
            {
                MetaTitle = "Список пользователей",
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список пользователей"
                    }
                }
            };

            var users = await _userManager.GetUsersAsync(
                pageIndex,
                PageSize,
                x => x.Id,
                orderDirection: OrderDirection.Descending
            );

            return View(
                new IndexViewModel
                {
                    Users = users,
                    Pagination = new Pagination
                    {
                        UrlTemplate = Url.Action("Index", new {pageIndex = "PAGE_INDEX"}),
                        PageSize = PageSize,
                        CurrentPageNumber = pageIndex,
                        TotalItems = users.TotalCount
                    }
                }
            );
        }

        public async Task<ActionResult> Details(int id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return HttpNotFound();

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Информация о пользователе - " + user.UserName,
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список пользователей"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Details", new {id}),
                        Text = "Информация о пользователе"
                    }
                }
            };

            return View(new DetailsViewModel
            {
                User = user,
                Roles = await _userManager.GetRolesAsync(user.Id),
                Accounts = _clientAccountService.Get(
                    1,
                    20,
                    x => x.Id,
                    OrderDirection.Ascending,
                    x => x.User.Id == id
                )
            });
        }

        public async Task<ActionResult> Create()
        {
            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Добавить нового пользователя",
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список пользователей"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Create"),
                        Text = "Добавить нового пользователя"
                    }
                }
            };

            var userRolesAvailable = await _roleManager.GetRolesAsync();

            return View(
                new EditViewModel
                {
                    RolesListAvailable = userRolesAvailable.Select(
                        x => new SelectListItem
                        {
                            Selected = false,
                            Text = x.Name,
                            Value = x.Name
                        })
                }
            );
        }

        //
        // POST: /Users/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Exclude = "RolesListAvailable")] EditViewModel request)
        {
            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Добавить нового пользователя",
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список пользователей"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Create"),
                        Text = "Добавить нового пользователя"
                    }
                }
            };

            if (request.RolesListSelect == null) request.RolesListSelect = new List<string>();

            if (ModelState.IsValid)
            {
                var user = new AppUser {UserName = request.Email, Email = request.Email};
                if (request.Password == null) request.Password = Membership.GeneratePassword(12, 1);

                var adminResult = await _userManager.CreateAsync(user, request.Password);
                if (adminResult.Succeeded)
                {
                    await _userManager.SetUserRolesAsync(user.Id, request.RolesListSelect);

                    _userManager.SetLockoutEnabled(user.Id, request.Locked);

                    return RedirectToAction("Details", new {user.Id});
                }

                ModelState.AddModelError("", adminResult.Errors.First());
            }

            var userRolesAvailable = await _roleManager.GetRolesAsync();
            request.RolesListAvailable = userRolesAvailable.Select(
                x => new SelectListItem
                {
                    Selected = request.RolesListSelect.Contains(x.Name),
                    Text = x.Name,
                    Value = x.Name
                });
            return View(request);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user == null) return HttpNotFound();

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Редактирование пользователя - " + user.UserName,
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список пользователей"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Edit", new {user.Id}),
                        Text = "Редактирование пользователя"
                    }
                }
            };

            var userRoles = await _userManager.GetRolesAsync(user.Id);
            var userRolesAvailable = await _roleManager.GetRolesAsync();

            return View(new EditViewModel
            {
                Id = user.Id,
                Email = user.Email,
                Locked = user.LockoutEnabled,
                RolesListAvailable = userRolesAvailable.Select(
                    x => new SelectListItem
                    {
                        Selected = userRoles.Contains(x.Name),
                        Text = x.Name,
                        Value = x.Name
                    })
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Exclude = "RolesListAvailable")] EditViewModel request)
        {
            var user = await _userManager.FindByIdAsync(request.Id);
            if (user == null) return HttpNotFound();

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Редактирование пользователя - " + user.UserName,
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список пользователей"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Edit", new {user.Id}),
                        Text = "Редактирование пользователя"
                    }
                }
            };

            var showForm = false;

            if (ModelState.IsValid)
            {
                user.UserName = request.Email;
                user.Email = request.Email;
                user.LockoutEnabled = request.Locked;

                if (request.RolesListSelect == null) request.RolesListSelect = new List<string>();

                var result = await _userManager.SetUserRolesAsync(user.Id, request.RolesListSelect);

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    showForm = true;
                }
            }
            else
            {
                showForm = true;
            }


            if (showForm)
            {
                var userRoles = await _userManager.GetRolesAsync(user.Id);
                var userRolesAvailable = await _roleManager.GetRolesAsync();
                request.RolesListAvailable = userRolesAvailable.Select(
                    x => new SelectListItem
                    {
                        Selected = userRoles.Contains(x.Name),
                        Text = x.Name,
                        Value = x.Name
                    });
                return View(request);
            }

            return RedirectToAction("Details", new {user.Id});
        }

        public async Task<ActionResult> Delete(int id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return HttpNotFound();

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Удалить пользователя " + user.UserName
            };

            return View(user.ToAppUser());
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int? id)
        {
            if (ModelState.IsValid)
            {
                if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                var result = await _userManager.DeleteAsync(id.Value);
                if (result.Errors.Contains("Invalid user Id")) return HttpNotFound();
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }

                return RedirectToAction("Index");
            }

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _userManager?.Dispose();
                _userManager = null;

                _roleManager?.Dispose();
                _roleManager = null;

                _clientAccountService?.Dispose();
                _clientAccountService = null;
            }

            base.Dispose(disposing);
        }
    }
}
