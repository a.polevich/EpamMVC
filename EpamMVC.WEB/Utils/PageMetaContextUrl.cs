﻿namespace EpamMVC.WEB.Utils
{
    public class PageMetaContextUrl
    {
        public string Link { get; set; }
        public string Text { get; set; }
    }
}
