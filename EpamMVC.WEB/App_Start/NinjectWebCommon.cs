﻿using System;
using System.Web;
using System.Web.Mvc;
using EpamMVC.WEB;
using EpamMVC.WEB.Dependencies;
using EpamMVC.WEB.Filters;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using Ninject.Web.Mvc.FilterBindingSyntax;
using WebActivatorEx;

[assembly:
    WebActivatorEx.PreApplicationStartMethod(
        typeof(NinjectWebCommon),
        "Start"
    )
]
[assembly:
    ApplicationShutdownMethod(
        typeof(NinjectWebCommon),
        "Stop"
    )
]

namespace EpamMVC.WEB
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

        /// <summary>
        ///     Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            Bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        ///     Stops the application.
        /// </summary>
        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }

        /// <summary>
        ///     Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel(
                new Dependencies.Data(),
                new Business(),
                new Identity()
            );
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                kernel.BindFilter<ExceptionFilterAttribute>(FilterScope.Global, 1).InRequestScope();

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }
    }
}
