﻿using System.Reflection;
using AutoMapper;

namespace EpamMVC.WEB.Utils
{
    public static class MapperEngine
    {
        private static readonly IMapper MapperInstance = new AutoMapper.Mapper(
            new MapperConfiguration(
                cfg => cfg.AddMaps(
                    Assembly.GetExecutingAssembly()
                )
            )
        );

        public static TTarget Map<TTarget>(object entity)
        {
            return MapperInstance.Map<TTarget>(entity);
        }
    }
}
