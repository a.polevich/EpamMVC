﻿namespace EpamMVC.WEB.Models.Layout
{
    public class HeaderViewModel
    {
        public string SiteName { get; set; }
        public string LogoUrl { get; set; }
    }
}
