using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Models;

namespace EpamMVC.WEB.Areas.Admin.Models.HomeAdmin
{
    public class IndexViewModel
    {

        public PaginatedList<ApplicationIdentityUser> LastUsers { get; set; }
        public PaginatedList<ClientAccount> ClientAccountsUnlockRequests { get; set; }
        public int ClientAccountCount { get; set; }
        public PaginatedList<ExceptionLog> ExceptionLogs { get; set; }
        public PaginatedList<ClientPayment> ClientPayments { get; set; }
        public PaginatedList<CallbackRequest> CallbackRequests { get; set; }
    }
}
