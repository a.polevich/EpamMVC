﻿namespace EpamMVC.DLL.Models.Identity
{
    public sealed class ApplicationUserLoginInfo
    {
        public ApplicationUserLoginInfo(string loginProvider, string providerKey)
        {
            LoginProvider = loginProvider;
            ProviderKey = providerKey;
        }

        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }
}
