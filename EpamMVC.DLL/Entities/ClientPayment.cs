using System;
using EpamMVC.DLL.Interfaces;

namespace EpamMVC.DLL.Entities
{
    public class ClientPayment : IEntity
    {
        public enum Statuses
        {
            Prepare,
            Send
        }

        public int Id { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public DateTime? SendDateTime { get; set; }
        public Statuses Status { get; set; }
        public int AccountId { get; set; }
        public ClientAccount Account { get; set; }
        public decimal Amount { get; set; }
    }
}
