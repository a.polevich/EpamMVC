﻿using EpamMVC.DLL.Entities;

namespace EpamMVC.BLL.Interfaces
{
    public interface IClientPaymentService : IService<ClientPayment>
    {
    }
}
