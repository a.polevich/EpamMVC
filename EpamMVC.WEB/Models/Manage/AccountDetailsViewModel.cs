﻿using System.Collections.Generic;
using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Models;

namespace EpamMVC.WEB.Models.Manage
{
    public class AccountDetailsViewModel
    {
        public ClientAccount ClientAccount { get; set; }
        public PaginatedList<ClientPayment> AccountPayments { get; set; }
        public Pagination Pagination { get; set; }
        public Dictionary<string, string> SorterLinks { get; set; }
        public string CurrentSortingLabel { get; set; }
    }
}
