﻿using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Interfaces;

namespace EpamMVC.BLL.Services
{
    public class ProductCategoryService : Service<ProductCategory>, IProductCategoryService
    {
        public ProductCategoryService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
