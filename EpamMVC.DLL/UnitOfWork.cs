﻿using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using EpamMVC.DLL.Interfaces;

namespace EpamMVC.DLL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDataContext _context;
        private readonly Hashtable _repositories = new Hashtable();

        private bool _disposed;

        public UnitOfWork(IDataContext context)
        {
            _context = context;
        }

        public IRepository<TEntity> Repository<TEntity>() where TEntity : class, IEntity
        {
            var entityTypeName = typeof(TEntity).Name;

            if (!_repositories.ContainsKey(entityTypeName))
                _repositories.Add(entityTypeName, new Repository<TEntity>(_context));

            return (IRepository<TEntity>) _repositories[entityTypeName];
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return _context.SaveChangesAsync(cancellationToken);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                _disposed = true;
                _context.Dispose();
                foreach (IDisposable repository in _repositories.Values) repository.Dispose();
            }
        }
    }
}
