﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Interfaces;
using EpamMVC.WEB.Areas.Admin.Models.ExceptionLogAdmin;
using EpamMVC.WEB.Models;
using EpamMVC.WEB.Utils;

namespace EpamMVC.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "Администраторы")]
    public class ExceptionLogAdminController : Controller
    {
        private readonly IExceptionLogService _exceptionLogService;
        private readonly int _pageSize = 20;

        public ExceptionLogAdminController(IExceptionLogService exceptionLogService)
        {
            _exceptionLogService = exceptionLogService;
        }

        public async Task<ActionResult> Index(int pageIndex = 1)
        {
            var items = await _exceptionLogService.GetAsync(
                pageIndex,
                _pageSize,
                x => x.Id,
                OrderDirection.Descending
            );

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Журнал ошибок"
            };

            return View(
                new IndexViewModel
                {
                    LogRecords = items,
                    Pagination = new Pagination
                    {
                        UrlTemplate = Url.Action("Index", new {pageIndex = "PAGE_INDEX"}),
                        PageSize = _pageSize,
                        CurrentPageNumber = pageIndex,
                        TotalItems = items.TotalCount
                    }
                }
            );
        }

        public async Task<ActionResult> Details(int id)
        {
            var record = await _exceptionLogService.GetSingleAsync(x => x.Id == id);
            if (record == null) return HttpNotFound();

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Информация о ошибке - " + record.Id,
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Журнал об ошибках"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Details", new {id}),
                        Text = "Информация о ошибке"
                    }
                }
            };

            return View(new DetailsViewModel
            {
                LogRecord = record
            });
        }
    }
}
