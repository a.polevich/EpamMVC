﻿using System.Threading.Tasks;
using System.Web.Mvc;
using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Interfaces;
using EpamMVC.WEB.Areas.Admin.Models.HomeAdmin;
using EpamMVC.WEB.Utils;

namespace EpamMVC.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "Администраторы")]
    public class HomeAdminController : Controller
    {
        private const int lastCount = 5;
        private IClientAccountService _clientAccountService;
        private IClientPaymentService _clientPaymentService;
        private ICallbackService _callbackService;
        private IExceptionLogService _exceptionLogService;
        private IApplicationUserManager _userManager;

        public HomeAdminController(
            IApplicationUserManager userManager,
            IClientAccountService clientAccountService,
            IClientPaymentService clientPaymentService,
            ICallbackService callbackService,
            IExceptionLogService exceptionLogService
        )
        {
            _userManager = userManager;
            _clientAccountService = clientAccountService;
            _clientPaymentService = clientPaymentService;
            _callbackService = callbackService;
            _exceptionLogService = exceptionLogService;
        }

        public async Task<ActionResult> Index()
        {
            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Панель администратора",
                PageHeader = "Панель администратора"
            };

            return View(new IndexViewModel()
            {
                ClientAccountsUnlockRequests = await _clientAccountService.GetAsync(
                    1,
                    lastCount,
                    x => x.Id,
                    OrderDirection.Descending,
                    null,
                    x => x.User
                ),
                ClientAccountCount = await _clientAccountService.CountAsync(),
                CallbackRequests = await _callbackService.GetAsync(
                    1,
                    lastCount,
                    x => x.Id,
                    OrderDirection.Descending,
                    x => x.Called == false
                ),
                ClientPayments = await _clientPaymentService.GetAsync(
                    1,
                    lastCount,
                    x => x.Id,
                    OrderDirection.Descending
                ),
                ExceptionLogs = await _exceptionLogService.GetAsync(
                    1,
                    lastCount,
                    x => x.Id,
                    OrderDirection.Descending
                ),
                LastUsers = await _userManager.GetUsersAsync(
                    1,
                    lastCount,
                    x => x.Id,
                    OrderDirection.Descending
                )
            });
        }
    }
}
