using EpamMVC.DLL.Interfaces;

namespace EpamMVC.DLL.Entities
{
    public class Order : IEntity
    {
//        [Required]
//        [StringLength(256, MinimumLength = 4)]
        public string Title { get; set; }

//        [Required]
        public string ClientName { get; set; }

        public string ClientEmail { get; set; }
        public string ClientPhone { get; set; }


//        [Required]
//        [Range(1, 5)]
        public int DifficultyLevel { get; set; }

//        [Required]
//        [DataType(DataType.MultilineText)]
//        [StringLength(4048, MinimumLength = 16)]
        public string Description { get; set; }
        public int Id { get; set; }
    }
}
