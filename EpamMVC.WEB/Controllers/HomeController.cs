﻿using System.Web.Mvc;
using EpamMVC.WEB.Models.Home;

namespace EpamMVC.WEB.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.ShowHeader = false;

            return View(
                new IndexViewModel());
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}
