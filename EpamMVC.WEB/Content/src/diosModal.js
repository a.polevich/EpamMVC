﻿

let diosModal = function (options, element) {

    if (typeof options === "string") {
        if (options === "show") {
            $(element).modal("show");
        }
        return
    }

    const settings = $.extend({
        title: "",
        body: "",
        footer: "",
        width: '',
        autofocus: true,
        vertical_align: false,
        removeOnClose: false,
        show: true,
        class: '',
        padding: true,
        large: false
    }, options);

    const $modal = $(
        '<div class="modal fade" tabindex="-1" role="dialog">' +
        '<div class="modal-dialog" role="document">' +
        '<div class="modal-content">' +

        '<div class="modal-header">' +
        '<h5 class="modal-title"></h5>' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
        '<span aria-hidden="true">&times;</span>' +
        '</button>' +
        '</div>' +

        '<div class="modal-body"></div>' +

        '</div>' +
        '</div>' +
        '</div>'
    );

    if (settings.vertical_align) {
        $(".modal-dialog", $modal).addClass('modal-dialog-centered');
    }

    if (settings.large) {
        $(".modal-dialog", $modal).addClass('modal-lg');
    }

    if (settings.class) {
        $modal.addClass(settings.class);
    }

    if (settings.padding === false) {
        $modal.addClass('noPadding');
    }

    $('.modal-title', $modal).text(settings.title);

    $('.modal-body', $modal).html(settings.body);

    if (settings.footer !== '') {
        $('<div class="modal-footer"></div>').appendTo($('.modal-content', $modal)).append(settings.footer);
    }

    if (settings.autofocus) {
        $modal
            .on('shown.bs.modal', function () {
                $('input:text:visible, textarea:visible', $modal).eq(0).focus();
            });
    }

    $('.modal-close', $modal).on('click', function () {
        $modal.modal('hide');
    });

    if (settings.removeOnClose) {
        $modal.on('hidden.bs.modal', function () {
            $modal.removeData('modal').remove();
        });
    }

    $modal.modal({
        show: settings.show
    });

    return $modal;

};

export default diosModal;
