﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EpamMVC.WEB.Areas.Admin.Models.UserAdmin
{
    public class EditViewModel
    {
        public int Id { get; set; }

        public bool Locked { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Почтовый адрес необходим")]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        public string Password { get; set; }

        public IEnumerable<SelectListItem> RolesListAvailable { get; set; }
        public List<string> RolesListSelect { get; set; }
    }
}
