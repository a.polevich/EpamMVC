﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Interfaces;
using EpamMVC.WEB.Areas.Admin.Models.ClientAccountAdmin;
using EpamMVC.WEB.Models;
using EpamMVC.WEB.Utils;

namespace EpamMVC.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "Администраторы")]
    public class ClientAccountAdminController : Controller
    {
        private const int PageSize = 20;
        private readonly IClientAccountService _clientAccountService;

        public ClientAccountAdminController(IClientAccountService clientAccountService)
        {
            _clientAccountService = clientAccountService;
        }

        public async Task<ActionResult> Index(int pageIndex = 1)
        {
            var items = await _clientAccountService.GetAsync(
                pageIndex,
                PageSize,
                x => x.Id,
                OrderDirection.Descending,
                null,
                x => x.User
            );

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Карты пользователей",
                PageHeader = "Карты пользователей",
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Карты пользователей"
                    }
                }
            };

            return View(new IndexViewModel
            {
                Accounts = items,
                Pagination = new Pagination
                {
                    UrlTemplate = Url.Action("Index", new {pageIndex = "PAGE_INDEX"}),
                    PageSize = PageSize,
                    CurrentPageNumber = pageIndex,
                    TotalItems = items.TotalCount
                }
            });
        }

        public async Task<ActionResult> Edit(int id)
        {
            var account = await _clientAccountService.GetSingleAsync(x => x.Id == id);

            if (account == null) return HttpNotFound();

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Редактирование карты - " + account.Id,
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список карт"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Edit", new {account.Id}),
                        Text = "Редактирование карты"
                    }
                }
            };

            return View(account);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ClientAccount submittedAccount)
        {
            var currentAccount = await _clientAccountService.GetByIdAsync(submittedAccount.Id);

            if (ModelState.IsValid)
            {
                currentAccount.Balance = submittedAccount.Balance;
                currentAccount.Locked = submittedAccount.Locked;
                currentAccount.UnlockRequest = submittedAccount.UnlockRequest;

                if (currentAccount.UnlockRequest)
                {
                    currentAccount.UnlockRequest = true;
                }

                await _clientAccountService.UpdateAsync(currentAccount);
                return RedirectToAction("Index");
            }

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Редактирование карты - " + currentAccount.Id,
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список карт"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Edit", new {currentAccount.Id}),
                        Text = "Редактирование карты"
                    }
                }
            };

            return View(submittedAccount);
        }

        public async Task<ActionResult> Delete(int id)
        {
            var clientAccount = await _clientAccountService
                .GetSingleAsync(x => x.Id == id, x => x.User);
            if (clientAccount == null) return HttpNotFound();

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Удалить счет/карту " + clientAccount.Id,
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Карты пользователей"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Delete", new {clientAccount.Id}),
                        Text = "Удалить счет"
                    }
                }
            };

            return View(clientAccount);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            if (ModelState.IsValid)
            {
                var clientAccount = await _clientAccountService.GetSingleAsync(x => x.Id == id);
                if (clientAccount == null) return HttpNotFound();

                await _clientAccountService.DeleteAsync(clientAccount);

                return RedirectToAction("Index");
            }

            return View();
        }
    }
}
