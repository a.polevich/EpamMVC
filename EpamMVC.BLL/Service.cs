﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Interfaces;
using EpamMVC.DLL.Models;

namespace EpamMVC.BLL
{
    public class Service<TEntity> : IService<TEntity>
        where TEntity : class, IEntity
    {
        private readonly IRepository<TEntity> _repository;
        private bool _disposed;

        public Service(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
            _repository = UnitOfWork.Repository<TEntity>();
        }

        public IUnitOfWork UnitOfWork { get; }

        public PaginatedList<TEntity> Get(
            int pageIndex,
            int pageSize,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return _repository.Get(pageIndex, pageSize, predicate, includeProperties);
        }

        public PaginatedList<TEntity> Get<TKey>(
            int pageIndex,
            int pageSize,
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return _repository.Get(pageIndex, pageSize, keySelector, orderDirection, predicate, includeProperties);
        }

        public List<TEntity> GetAll(
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return _repository.GetAll(predicate, includeProperties);
        }

        public List<TEntity> GetAll<TKey>(
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return _repository.GetAll(keySelector, orderDirection, predicate, includeProperties);
        }

        public int Count()
        {
            return _repository.Count();
        }

        public int Count(Expression<Func<TEntity, bool>> predicate)
        {
            return _repository.Count(predicate);
        }

        public TEntity GetById(int id)
        {
            return _repository.GetSingle(x => x.Id == id);
        }

        public TEntity GetSingle(Expression<Func<TEntity, bool>> predicate)
        {
            return _repository.GetSingle(predicate);
        }

        public void Add(TEntity entity)
        {
            _repository.Insert(entity);
            UnitOfWork.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            _repository.Update(entity);
            UnitOfWork.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            _repository.Delete(entity);
            UnitOfWork.SaveChanges();
        }

        public Task<PaginatedList<TEntity>> GetAsync(
            int pageIndex,
            int pageSize,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return GetAsync(pageIndex, pageSize, x => x.Id, OrderDirection.Ascending, predicate, includeProperties);
        }

        public Task<PaginatedList<TEntity>> GetAsync<TKey>(
            int pageIndex,
            int pageSize,
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return _repository.GetAsync(pageIndex, pageSize, keySelector, orderDirection, predicate, includeProperties);
        }

        public Task<List<TEntity>> GetAllAsync(
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return _repository.GetAllAsync(predicate, includeProperties);
        }

        public Task<List<TEntity>> GetAllAsync<TKey>(
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return _repository.GetAllAsync(keySelector, orderDirection, predicate, includeProperties);
        }

        public Task<int> CountAsync()
        {
            return _repository.CountAsync();
        }

        public Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _repository.CountAsync(predicate);
        }

        public Task<TEntity> GetByIdAsync(
            int id,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return _repository.GetByIdAsync(id, includeProperties);
        }

        public Task<TEntity> GetSingleAsync(
            Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return _repository.GetSingleAsync(predicate, includeProperties);
        }

        public Task AddAsync(TEntity entity)
        {
            _repository.Insert(entity);
            return UnitOfWork.SaveChangesAsync();
        }

        public Task UpdateAsync(TEntity entity)
        {
            _repository.Update(entity);
            return UnitOfWork.SaveChangesAsync();
        }

        public Task DeleteAsync(TEntity entity)
        {
            _repository.Delete(entity);
            return UnitOfWork.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing) UnitOfWork.Dispose();

            _disposed = true;
        }
    }
}
