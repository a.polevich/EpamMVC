using System;
using EpamMVC.DLL.Interfaces;

namespace EpamMVC.DLL.Entities
{
    [Obsolete]
    public class File : IEntity
    {
        public string OriginalName { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileSize { get; set; }
        public DateTime? DateCreated { get; set; }
        public int Id { get; set; }
    }
}
