﻿namespace EpamMVC.WEB.Models.Callback
{
    public class CallbackRequestAjaxSubmit
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Url { get; set; }
    }
}
