﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EpamMVC.DLL.Interfaces;
using EpamMVC.DLL.Models;

namespace EpamMVC.BLL.Interfaces
{
    public interface IService<TEntity> : IDisposable where TEntity : class, IEntity
    {
        PaginatedList<TEntity> Get(
            int pageIndex,
            int pageSize,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        );

        PaginatedList<TEntity> Get<TKey>(
            int pageIndex,
            int pageSize,
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        );

        List<TEntity> GetAll(
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        );

        List<TEntity> GetAll<TKey>(
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        );

        int Count();
        int Count(Expression<Func<TEntity, bool>> predicate);

        TEntity GetById(int id);
        TEntity GetSingle(Expression<Func<TEntity, bool>> predicate);

        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);

        Task<PaginatedList<TEntity>> GetAsync(
            int pageIndex,
            int pageSize,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        );

        Task<PaginatedList<TEntity>> GetAsync<TKey>(
            int pageIndex,
            int pageSize,
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        );

        Task<List<TEntity>> GetAllAsync(
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        );

        Task<List<TEntity>> GetAllAsync<TKey>(
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        );

        Task<int> CountAsync();
        Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> GetByIdAsync(
            int id,
            params Expression<Func<TEntity, object>>[] includeProperties
        );

        Task<TEntity> GetSingleAsync(
            Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties
        );

        Task AddAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
    }
}
