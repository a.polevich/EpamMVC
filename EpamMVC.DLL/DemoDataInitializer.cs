﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using EpamMVC.Data.Identity;
using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Models;
using Microsoft.AspNet.Identity;

namespace EpamMVC.DLL
{
    public class DemoDataInitializer : DropCreateDatabaseIfModelChanges<DataContext>
    {
        private static readonly Random RandomInstance = new Random();

        protected override void Seed(DataContext db)
        {
            SeedUserAdmin(db);
            SeedCallbackRequests(db);
            SeedUserClients(db);
            SeedProducts(db);
            SeedExceptionLog(db);
            base.Seed(db);

            db.SaveChanges();
        }

        private static void SeedExceptionLog(DataContext db)
        {
            for (var i = 1; i <= 32; i++)
            {
                db.Set<ExceptionLog>().Add(new ExceptionLog()
                {
                    ControllerName = "Demo",
                    ActionName = "Demo",
                    DateTime = DateTime.Now,
                    Exception = "Demo exception",
                    StackTrace = "Just demo exception"
                });
            }
        }

        private static void SeedProducts(DataContext db)
        {
            for (var i = 1; i <= 16; i++)
            {
                var category = new ProductCategory
                {
                    Name = "Категория " + i,
                    Description =
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ac turpis vitae nibh fringilla sagittis id sit amet quam. Vivamus vestibulum neque in ipsum dignissim lobortis. Curabitur ullamcorper viverra nunc, sed congue turpis ullamcorper a. Pellentesque vulputate mollis rhoncus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas vestibulum cursus lectus a sollicitudin. Pellentesque id lacus interdum, scelerisque nunc pellentesque, sollicitudin metus. Donec consectetur felis eu justo tempus, et ultrices enim vulputate. Aenean sed congue mauris, in luctus turpis. In pellentesque porttitor gravida. In facilisis mi id tellus iaculis pellentesque."
                };

                db.Set<ProductCategory>().Add(category);

                for (var j = 0; j < 32; j++)
                    db.Set<Product>().Add(new Product
                    {
                        Category = category,
                        Name = "Товар " + j,
                        Description =
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ac turpis vitae nibh fringilla sagittis id sit amet quam. Vivamus vestibulum neque in ipsum dignissim lobortis. Curabitur ullamcorper viverra nunc, sed congue turpis ullamcorper a. Pellentesque vulputate mollis rhoncus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas vestibulum cursus lectus a sollicitudin. Pellentesque id lacus interdum, scelerisque nunc pellentesque, sollicitudin metus. Donec consectetur felis eu justo tempus, et ultrices enim vulputate. Aenean sed congue mauris, in luctus turpis. In pellentesque porttitor gravida. In facilisis mi id tellus iaculis pellentesque.",
                        Price = RandomInstance.Next(300, 2000)
                    });
            }

            db.SaveChanges();
        }

        private static void SeedCallbackRequests(DataContext db)
        {
            for (var i = 1; i <= 256; i++)
                db.Set<CallbackRequest>().Add(new CallbackRequest
                {
                    CreatedDateTime = DateTime.Now,
                    Called = RandomInstance.NextDouble() >= 0.5,
                    Name = "Клиент " + i,
                    Phone = "8 (000) 000-00-00"
                });
        }

        private static void SeedUserAdmin(DataContext db)
        {
            const string name = "admin@easy-travel.com";
            const string password = "VsfD~htY3Zy6%JYm";
            const string roleName = "Администраторы";
            var applicationRoleManager = IdentityFactory.CreateRoleManager(db);
            var applicationUserManager = IdentityFactory.CreateUserManager(db);

            //Create Role Admin if it does not exist
            var userAdminRole = applicationRoleManager.FindByName(roleName);
            if (userAdminRole == null)
            {
                userAdminRole = new ApplicationIdentityRole {Name = roleName};
                applicationRoleManager.Create(userAdminRole);
            }

            var adminUser = applicationUserManager.FindByName(name);
            if (adminUser == null)
            {
                adminUser = new ApplicationIdentityUser {UserName = name, Email = name};
                applicationUserManager.Create(adminUser, password);
                applicationUserManager.SetLockoutEnabled(adminUser.Id, false);
            }

            // Add user admin to Role Admin if not already added
            var rolesForUser = applicationUserManager.GetRoles(adminUser.Id);
            if (!rolesForUser.Contains(userAdminRole.Name))
                applicationUserManager.AddToRole(adminUser.Id, userAdminRole.Name);


            for (var i = 0; i < 8; i++)
            {
                var account = new ClientAccount
                {
                    User = adminUser,
                    Balance = RandomInstance.Next(0, 10000),
                    Locked = RandomInstance.NextDouble() > 0.5
                };

                if (account.Locked) account.UnlockRequest = RandomInstance.NextDouble() > 0.5;

                db.Set<ClientAccount>().Add(account);

                for (var j = 0; j < 16; j++)
                    db.Set<ClientPayment>().Add(new ClientPayment
                    {
                        Account = account,
                        Amount = RandomInstance.Next(0, 1000),
                        Status = RandomEnumValue<ClientPayment.Statuses>(),
                        CreatedDateTime = DateTime.Now.AddMonths(-6).AddDays(RandomInstance.Next(10)),
                        SendDateTime = DateTime.Now.AddMonths(-6).AddDays(RandomInstance.Next(12, 30))
                    });
            }

            db.SaveChanges();
        }

        private static void SeedUserClients(DataContext db)
        {
            var applicationUserManager = IdentityFactory.CreateUserManager(db);
            var applicationRoleManager = IdentityFactory.CreateRoleManager(db);

            var roles = new List<ApplicationIdentityRole>();

            for (var i = 0; i < 8; i++)
                roles.Add(new ApplicationIdentityRole
                {
                    Name = "Демо роль " + i
                });

            foreach (var role in roles) applicationRoleManager.Create(role);

            for (var i = 0; i < 32; i++)
            {
                var userLogin = $"demouser{i}@gmail.com";
                var user = new ApplicationIdentityUser
                {
                    UserName = userLogin,
                    Email = userLogin
                };

                var result = applicationUserManager.Create(user, userLogin);

                if (result.Succeeded)
                {
                    if (RandomInstance.NextDouble() > 0.5) applicationUserManager.SetLockoutEnabled(user.Id, false);

                    for (var j = 0; j < 4; j++)
                    {
                        var account = new ClientAccount
                        {
                            User = user,
                            Balance = RandomInstance.Next(0, 10000),
                            Locked = RandomInstance.NextDouble() > 0.5
                        };

                        if (account.Locked) account.UnlockRequest = RandomInstance.NextDouble() > 0.5;

                        db.Set<ClientAccount>().Add(account);

                        for (var y = 0; y < 16; y++)
                            db.Set<ClientPayment>().Add(new ClientPayment
                            {
                                Account = account,
                                Amount = RandomInstance.Next(0, 1000),
                                Status = RandomEnumValue<ClientPayment.Statuses>(),
                                CreatedDateTime = DateTime.Now.AddMonths(-6).AddDays(RandomInstance.Next(10)),
                                SendDateTime = DateTime.Now.AddMonths(-6).AddDays(RandomInstance.Next(12, 30))
                            });
                    }
                }

                applicationUserManager.AddToRole(
                    user.Id,
                    GetRandomVariant(roles).Name
                );
            }

            db.SaveChanges();
        }

        private static T GetRandomVariant<T>(T[] variants)
        {
            if (variants is null || variants.Length == 0) return default;
            return variants[RandomInstance.Next(variants.Length)];
        }

        private static T GetRandomVariant<T>(List<T> variants)
        {
            if (variants.Count == 0) return default;
            return variants[RandomInstance.Next(variants.Count)];
        }

        private static T RandomEnumValue<T>()
        {
            var v = Enum.GetValues(typeof(T));
            return (T) v.GetValue(RandomInstance.Next(v.Length));
        }

        private static T GetRandomVariant<T>(ICollection<T> variants)
        {
            return variants.Skip(RandomInstance.Next(variants.Count)).FirstOrDefault();
        }
    }
}
