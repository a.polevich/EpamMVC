﻿using EpamMVC.DLL.Entities;

namespace EpamMVC.BLL.Interfaces
{
    public interface IProductCategoryService : IService<ProductCategory>
    {
    }
}
