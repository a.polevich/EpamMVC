﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EpamMVC.DLL.Extensions;
using EpamMVC.DLL.Interfaces;
using EpamMVC.DLL.Models;

namespace EpamMVC.DLL
{
    public class Repository<TEntity> :
        IDisposable,
        IRepository<TEntity> where TEntity : class, IEntity
    {
        private readonly IDataContext _context;
        private readonly IDbSet<TEntity> _dbEntitySet;
        private bool _disposed;

        public Repository(IDataContext context)
        {
            _context = context;
            _dbEntitySet = _context.Set<TEntity>();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public PaginatedList<TEntity> Get(
            int pageIndex,
            int pageSize,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            if (pageIndex == 0) pageIndex = 1;
            if (pageSize == 0) pageSize = 20;

            var entities = FilterQuery(predicate, includeProperties);
            var total = entities.Count();
            entities = entities.Paginate(pageIndex, pageSize);
            return entities.ToPaginatedList(pageIndex, pageSize, total);
        }

        public PaginatedList<TEntity> Get<TKey>(
            int pageIndex,
            int pageSize,
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            if (pageIndex == 0) pageIndex = 1;
            if (pageSize == 0) pageSize = 20;

            var entities = FilterQuery(keySelector, orderDirection, predicate, includeProperties);
            var total = entities.Count();
            entities = entities.Paginate(pageIndex, pageSize);
            return entities.ToPaginatedList(pageIndex, pageSize, total);
        }

        public List<TEntity> GetAll(
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return FilterQuery(predicate, includeProperties).ToList();
        }

        public List<TEntity> GetAll<TKey>(
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return FilterQuery(keySelector, orderDirection, predicate, includeProperties).ToList();
        }

        public int Count()
        {
            return _dbEntitySet.Count();
        }

        public int Count(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbEntitySet.Count(predicate);
        }

        public TEntity GetById(int id)
        {
            return _dbEntitySet.FirstOrDefault(t => t.Id == id);
        }

        public TEntity GetSingle(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbEntitySet.FirstOrDefault(predicate);
        }

        public TEntity GetSingleIncluding(int id, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            return entities.FirstOrDefault(x => x.Id == id);
        }

        public List<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbEntitySet.Where(predicate).ToList();
        }

        public void Insert(TEntity entity)
        {
            _context.SetAsAdded(entity);
        }

        public void Update(TEntity entity)
        {
            _context.SetAsModified(entity);
        }

        public void Delete(TEntity entity)
        {
            _context.SetAsDeleted(entity);
        }

        public async Task<PaginatedList<TEntity>> GetAsync(
            int pageIndex,
            int pageSize,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            if (pageIndex == 0) pageIndex = 1;
            if (pageSize == 0) pageSize = 20;

            var entities = FilterQuery(predicate, includeProperties);
            var total = await entities.CountAsync(); // entities.CountAsync() is different than pageSize
            entities = entities.Paginate(pageIndex, pageSize);
            var list = await entities.ToListAsync();
            return list.ToPaginatedList(pageIndex, pageSize, total);
        }

        public async Task<PaginatedList<TEntity>> GetAsync<TKey>(
            int pageIndex,
            int pageSize,
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            if (pageIndex == 0) pageIndex = 1;
            if (pageSize == 0) pageSize = 20;

            var entities = FilterQuery(keySelector, orderDirection, predicate, includeProperties);
            var total = await entities.CountAsync(); // entities.CountAsync() is different than pageSize
            entities = entities.Paginate(pageIndex, pageSize);
            var list = await entities.ToListAsync();
            return list.ToPaginatedList(pageIndex, pageSize, total);
        }

        public async Task<List<TEntity>> GetAllAsync(
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return await FilterQuery(predicate, includeProperties).ToListAsync();
        }

        public async Task<List<TEntity>> GetAllAsync<TKey>(
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return await FilterQuery(keySelector, orderDirection, predicate, includeProperties).ToListAsync();
        }


        public Task<int> CountAsync()
        {
            return _dbEntitySet.CountAsync();
        }

        public Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbEntitySet.CountAsync(predicate);
        }

        public Task<TEntity> GetByIdAsync(
            int id,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return IncludeProperties(includeProperties).FirstOrDefaultAsync(t => t.Id == id);
        }

        public Task<TEntity> GetSingleAsync(
            Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            return IncludeProperties(includeProperties).FirstOrDefaultAsync(predicate);
        }

        public Task<TEntity> GetSingleIncludingAsync(
            int id,
            params Expression<Func<TEntity, object>>[] includeProperties
        )
        {
            var entities = IncludeProperties(includeProperties);
            return entities.FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<List<TEntity>> FindByAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbEntitySet.Where(predicate).ToListAsync();
        }

        private IQueryable<TEntity> FilterQuery(
            Expression<Func<TEntity, bool>> predicate = null,
            Expression<Func<TEntity, object>>[] includeProperties = null)
        {
            var entities = IncludeProperties(includeProperties);

            if (predicate != null) entities = entities.Where(predicate);

            return entities;
        }

        private IQueryable<TEntity> FilterQuery<TKey>(
            Expression<Func<TEntity, TKey>> keySelector,
            OrderDirection orderDirection = OrderDirection.Ascending,
            Expression<Func<TEntity, bool>> predicate = null,
            Expression<Func<TEntity, object>>[] includeProperties = null)
        {
            var entities = IncludeProperties(includeProperties);

            if (predicate != null) entities = entities.Where(predicate);

            if (keySelector != null)
                // https://stackoverflow.com/questions/39685787/how-to-make-a-dynamic-order-in-entity-framework
//                var selectorBody = keySelector.Body;
//                if (selectorBody.NodeType == ExpressionType.Convert)
//                    selectorBody = ((UnaryExpression) selectorBody).Operand;
//
//                var selector =
//                    (Expression<Func<TEntity, TKey>>)
//                    Expression.Lambda(selectorBody, keySelector.Parameters);

                entities = orderDirection == OrderDirection.Ascending
                    ? entities.OrderBy(keySelector)
                    : entities.OrderByDescending(keySelector);

            return entities;
        }

        private IQueryable<TEntity> IncludeProperties(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> entities = _dbEntitySet;
            foreach (var includeProperty in includeProperties) entities = entities.Include(includeProperty);
            return entities;
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing) _context.Dispose();
            _disposed = true;
        }
    }
}
