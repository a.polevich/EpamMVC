﻿using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Models;
using EpamMVC.WEB.Models;

namespace EpamMVC.WEB.Areas.Admin.Models.ExceptionLogAdmin
{
    public class IndexViewModel
    {
        public PaginatedList<ExceptionLog> LogRecords { get; set; }
        public Pagination Pagination { get; set; }
    }
}
