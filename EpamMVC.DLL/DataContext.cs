﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Interfaces;
using EpamMVC.DLL.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EpamMVC.DLL
{
    public class DataContext :
        IdentityDbContext<
            ApplicationIdentityUser,
            ApplicationIdentityRole,
            int,
            ApplicationIdentityUserLogin,
            ApplicationIdentityUserRole,
            ApplicationIdentityUserClaim
        >,
        IDataContext
    {
        private static readonly object Lock = new object();
        private static bool _databaseInitialized;

        public DataContext()
        {
            if (_databaseInitialized) return;

            lock (Lock)
            {
                if (!_databaseInitialized)
                {
                    _databaseInitialized = true;
                    Database.SetInitializer(new DemoDataInitializer());
                }
            }
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class, IEntity
        {
            return base.Set<TEntity>();
        }

        public void SetAsAdded<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            UpdateEntityState(entity, EntityState.Added);
        }

        public void SetAsModified<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            UpdateEntityState(entity, EntityState.Modified);
        }

        public void SetAsDeleted<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            UpdateEntityState(entity, EntityState.Deleted);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CallbackRequest>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ApplicationIdentityUser>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<ApplicationIdentityRole>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<ApplicationIdentityUserClaim>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ClientAccount>();
            modelBuilder.Entity<ClientPayment>();
            modelBuilder.Entity<ExceptionLog>();
            modelBuilder.Entity<Product>();
            modelBuilder.Entity<ProductCategory>();
        }

        private void UpdateEntityState<TEntity>(TEntity entity, EntityState entityState)
            where TEntity : class, IEntity
        {
            var dbEntityEntry = GetDbEntityEntrySafely(entity);
            dbEntityEntry.State = entityState;
        }

        private DbEntityEntry GetDbEntityEntrySafely<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            var dbEntityEntry = Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached) Set<TEntity>().Attach(entity);

            return dbEntityEntry;
        }
    }
}
