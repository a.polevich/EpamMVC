﻿using System.Data.Entity;
using System.Web;
using EpamMVC.Data.Identity;
using EpamMVC.DLL;
using EpamMVC.DLL.Identity;
using EpamMVC.DLL.Interfaces;
using EpamMVC.DLL.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;

namespace EpamMVC.WEB.Dependencies
{
    public class Identity : NinjectModule
    {
        public override void Load()
        {
            Bind<IApplicationUserManager>().To<ApplicationUserManager>().InRequestScope();
            Bind<IApplicationRoleManager>().To<ApplicationRoleManager>().InRequestScope();
            Bind<IUser<int>>().To<ApplicationIdentityUser>().InRequestScope();

            Bind<IDataContext>().To<DataContext>().InRequestScope();
            Bind<DbContext>().To<DataContext>().InRequestScope();

            Bind<UserManager<ApplicationIdentityUser, int>>()
                .ToMethod(
                    context =>
                    {
                        var manager = IdentityFactory.CreateUserManager(
                            context.Kernel.Get<DbContext>()
                        );

                        if (Startup.DataProtectionProvider != null)
                            manager.UserTokenProvider =
                                new DataProtectorTokenProvider<ApplicationIdentityUser, int>(
                                    Startup.DataProtectionProvider.Create("ASP.NET Identity"));

                        return manager;
                    }
                ).InRequestScope();

            Bind<RoleManager<ApplicationIdentityRole, int>>()
                .ToMethod(
                    context =>
                        IdentityFactory.CreateRoleManager(context.Kernel.Get<DbContext>())
                ).InRequestScope();

            Bind<IAuthenticationManager>()
                .ToMethod(
                    context => HttpContext.Current.GetOwinContext().Authentication
                ).InRequestScope();

// Autofac sample
//            builder.RegisterType(typeof(ApplicationUserManager)).As(typeof(IApplicationUserManager)).InstancePerHttpRequest();
//            builder.RegisterType(typeof(ApplicationRoleManager)).As(typeof(IApplicationRoleManager)).InstancePerHttpRequest();
//            builder.RegisterType(typeof(ApplicationIdentityUser)).As(typeof(IUser<int>)).InstancePerHttpRequest();
//            builder.Register(b => b.Resolve<IEntitiesContext>() as DbContext).InstancePerHttpRequest();
//            builder.Register(b =>
//            {
//                var manager = IdentityFactory.CreateUserManager(b.Resolve<DbContext>());
//                if (Startup.DataProtectionProvider != null)
//                {
//                    manager.UserTokenProvider =
//                        new DataProtectorTokenProvider<ApplicationIdentityUser, int>(
//                            Startup.DataProtectionProvider.Create("ASP.NET Identity"));
//                }
//                return manager;
//            }).InstancePerHttpRequest();
//            builder.Register(b => IdentityFactory.CreateRoleManager(b.Resolve<DbContext>())).InstancePerHttpRequest();
//            builder.Register(b => HttpContext.Current.GetOwinContext().Authentication).InstancePerHttpRequest();
        }
    }
}
