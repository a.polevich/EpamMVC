﻿using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Interfaces;

namespace EpamMVC.BLL.Services
{
    public class ClientAccountService : Service<ClientAccount>, IClientAccountService
    {
        public ClientAccountService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
