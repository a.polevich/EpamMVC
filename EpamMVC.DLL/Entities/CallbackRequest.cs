﻿using System;
using EpamMVC.DLL.Interfaces;

namespace EpamMVC.DLL.Entities
{
    public class CallbackRequest : IEntity
    {
        public bool Called { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public int Id { get; set; }
    }
}
