using EpamMVC.DLL.Interfaces;

namespace EpamMVC.DLL.Entities
{
    public class Product : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public ProductCategory Category { get; set; }

    }
}
