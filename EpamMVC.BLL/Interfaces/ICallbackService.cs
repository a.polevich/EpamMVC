﻿using EpamMVC.DLL.Entities;

namespace EpamMVC.BLL.Interfaces
{
    public interface ICallbackService : IService<CallbackRequest>
    {
    }
}
