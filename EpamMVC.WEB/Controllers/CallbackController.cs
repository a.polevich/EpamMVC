﻿using System;
using System.Web.Mvc;
using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Entities;
using EpamMVC.WEB.Models.Callback;
using EpamMVC.WEB.Utils;

namespace EpamMVC.WEB.Controllers
{
    public class CallbackController : Controller
    {
        private readonly ICallbackService _callbackService;

        public CallbackController(ICallbackService callbackService)
        {
            _callbackService = callbackService;
        }

        [HttpPost]
        public JsonResult SubmitCallbackRequestAjax(CallbackRequestAjaxSubmit clientCallbackRequest)
        {
            if (ModelState.IsValid)
            {
                var dbRequest = MapperEngine.Map<CallbackRequest>(clientCallbackRequest);
                dbRequest.CreatedDateTime = DateTime.Now;
                _callbackService.Add(dbRequest);

                return Json(new
                {
                    result = "success"
                });
            }

            return Json(new
            {
                result = "error"
            });
        }
    }
}
