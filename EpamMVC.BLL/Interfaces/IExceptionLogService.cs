﻿using EpamMVC.DLL.Entities;

namespace EpamMVC.BLL.Interfaces
{
    public interface IExceptionLogService : IService<ExceptionLog>
    {
    }
}
