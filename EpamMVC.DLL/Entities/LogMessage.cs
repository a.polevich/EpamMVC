﻿using System;
using EpamMVC.DLL.Interfaces;

namespace EpamMVC.DLL.Entities
{
    public class ExceptionLog : IEntity
    {
        public string Exception { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string StackTrace { get; set; }
        public DateTime DateTime { get; set; }
        public int Id { get; set; }
    }
}
