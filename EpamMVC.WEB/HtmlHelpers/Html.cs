using System.Linq;
using System.Web.Mvc;
using EpamMVC.WEB.Models;
using EpamMVC.WEB.Utils;

namespace EpamMVC.WEB.HtmlHelpers
{
    public static class Html
    {
        public static MvcHtmlString PrintAdminBreadscrumbs(this HtmlHelper html, PageMetaContext pageMetaContext)
        {
            if (
                pageMetaContext == null
                || !pageMetaContext.Breadscrumbs.Any()
            )
                return new MvcHtmlString("");


            var template = "<li class='breadcrumb-item'><a href='{0}'>{1}</a></li>";

            var bread = new TagBuilder("ol");

            bread.AddCssClass("breadcrumb float-sm-right");

            bread.InnerHtml += string.Format(
                template,
                new UrlHelper(html.ViewContext.RequestContext).Action(
                    "Index",
                    "HomeAdmin"
                ),
                "Главная"
            );

            foreach (var item in pageMetaContext.Breadscrumbs)
                bread.InnerHtml +=
                    string.Format(
                        template,
                        html.Encode(item.Link),
                        item.Text
                    );

            return new MvcHtmlString(bread.ToString());
        }

        public static MvcHtmlString PrintBreadscrumbs(this HtmlHelper html, PageMetaContext pageMetaContext)
        {
            if (
                pageMetaContext == null
                || pageMetaContext.Breadscrumbs.Count < 2
            )
                return new MvcHtmlString("");

            var template = "<li class='breadcrumb-item'><a href='{0}'>{1}</a></li>";

            var bread = new TagBuilder("ol");

            bread.AddCssClass("breadcrumb");

            foreach (var item in pageMetaContext.Breadscrumbs)
                bread.InnerHtml +=
                    string.Format(
                        template,
                        html.Encode(item.Link),
                        item.Text
                    );

            return new MvcHtmlString(bread.ToString());
        }


        public static MvcHtmlString PrintPagination(this HtmlHelper html, Pagination pagination)
        {
            if (
                pagination.TotalPages < 2
                || string.IsNullOrWhiteSpace(pagination.UrlTemplate)
                || string.IsNullOrWhiteSpace(pagination.PageIndexPlaceHolder)
            )
                return new MvcHtmlString("");

            var url = pagination.UrlTemplate.Replace(
                pagination.PageIndexPlaceHolder,
                "{0}"
            );

            var buttonLinkTemplate =
                $"<li class='page-item {{2}}'><a class='page-link' href='{url}'>{{1}}</a></li>";

            var htmlPagination = new TagBuilder("ul");
            htmlPagination.AddCssClass("pagination pagination-sm m-0");

            if (pagination.CurrentPageNumber > 2)
                htmlPagination.InnerHtml +=
                    string.Format(buttonLinkTemplate, 1, "<<", "first");

            if (pagination.CurrentPageNumber > 1)
                htmlPagination.InnerHtml +=
                    string.Format(buttonLinkTemplate, pagination.CurrentPageNumber - 1, "<", "prev");

            for (var i = pagination.CurrentPageNumber - 5; i < pagination.CurrentPageNumber; i++)
            {
                if (i < 1) continue;

                htmlPagination.InnerHtml += string.Format(buttonLinkTemplate, i, i, "");
            }

            htmlPagination.InnerHtml += string.Format(
                buttonLinkTemplate,
                pagination.CurrentPageNumber,
                pagination.CurrentPageNumber,
                "active"
            );

            for (
                var i = pagination.CurrentPageNumber + 1;
                i <= pagination.TotalPages && i <= pagination.CurrentPageNumber + 5;
                i++
            )
                htmlPagination.InnerHtml += string.Format(buttonLinkTemplate, i, i, "");

            if (pagination.CurrentPageNumber < pagination.TotalPages)
                htmlPagination.InnerHtml +=
                    string.Format(buttonLinkTemplate, pagination.CurrentPageNumber + 1, ">", "next");

            if (pagination.CurrentPageNumber < pagination.TotalPages - 1)
                htmlPagination.InnerHtml +=
                    string.Format(buttonLinkTemplate, pagination.TotalPages, ">>", "last");

            return new MvcHtmlString(htmlPagination.ToString());
        }
    }
}
