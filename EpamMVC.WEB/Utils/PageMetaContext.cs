﻿using System.Collections.Generic;

namespace EpamMVC.WEB.Utils
{
    public class PageMetaContext
    {
        private string _header = "";
        public List<PageMetaContextUrl> Breadscrumbs { get; set; } = new List<PageMetaContextUrl>();

        public string MetaTitle { get; set; } = "";
        public string MetaDescription { get; set; } = "";

        public string PageHeader
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_header)) return MetaTitle;

                return _header;
            }
            set => _header = value;
        }
    }
}
