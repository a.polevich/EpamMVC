﻿using EpamMVC.DLL.Models;

namespace EpamMVC.WEB.Areas.Admin.Models.Layout
{
    public class SidebarViewModel
    {
        public string UserFullName { get; set; }
        public ApplicationIdentityUser CurrentUser { get; set; }
    }
}
