﻿using System.Collections.Generic;
using System.Net;
using System.Web.Configuration;
using Newtonsoft.Json;

namespace EpamMVC.WEB.Utils
{
    public class Recaptcha
    {
        public static string SiteKey =>
            WebConfigurationManager.AppSettings["recaptchaSiteKey"];

        public static string SecretKey
            => WebConfigurationManager.AppSettings["recaptchaSecretKey"];

        public static CaptchaResponse ValidateCaptcha(string response)
        {
            var client = new WebClient();
            var jsonResult =
                client.DownloadString(
                    string.Format(
                        "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}",
                        SecretKey,
                        response
                    )
                );
            return JsonConvert.DeserializeObject<CaptchaResponse>(jsonResult);
        }
    }

    public class CaptchaResponse
    {
        [JsonProperty("success")] public bool Success { get; set; }
        [JsonProperty("error-codes")] public List<string> ErrorMessage { get; set; }
    }
}
