﻿namespace EpamMVC.DLL.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
