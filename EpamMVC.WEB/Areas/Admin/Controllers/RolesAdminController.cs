﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using EpamMVC.DLL.Interfaces;
using EpamMVC.DLL.Models.Identity;
using EpamMVC.WEB.Areas.Admin.Models.UserAdmin;
using EpamMVC.WEB.Utils;

namespace EpamMVC.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "Администраторы")]
    public class RolesAdminController : Controller
    {
        private IApplicationRoleManager _roleManager;
        private IApplicationUserManager _userManager;

        public RolesAdminController(IApplicationUserManager userManager, IApplicationRoleManager roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        //
        // GET: /Roles/
        public ActionResult Index()
        {
            ViewBag.PageMeta = new PageMetaContext
            {
                MetaTitle = "Список ролей пользователей",
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список ролей"
                    }
                }
            };

            return View(_roleManager.GetRoles());
        }

        //
        // GET: /Roles/Create
        public ActionResult Create()
        {
            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Добавить новую роль для пользователей",
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список ролей"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Create"),
                        Text = "Добавить роль"
                    }
                }
            };

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(RoleViewModel roleViewModel)
        {
            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Добавить новую роль для пользователей",
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список ролей"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Create"),
                        Text = "Добавить роль"
                    }
                }
            };

            if (ModelState.IsValid)
            {
                var role = new ApplicationRole {Name = roleViewModel.Name};
                var roleresult = await _roleManager.CreateAsync(role);
                if (!roleresult.Succeeded)
                {
                    ModelState.AddModelError("", roleresult.Errors.First());
                    return View();
                }

                return RedirectToAction("Index");
            }

            return View();
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var role = await _roleManager.FindByIdAsync(id.Value);
            if (role == null) return HttpNotFound();

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Редактирование роли - " + role.Name,
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список роли"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Edit", new {role.Id}),
                        Text = "Редактирование роли"
                    }
                }
            };

            var roleModel = new RoleViewModel {Id = role.Id, Name = role.Name};
            return View(roleModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Name,Id")] RoleViewModel roleModel)
        {
            if (ModelState.IsValid)
            {
                await _roleManager.UpdateAsync(roleModel.Id, roleModel.Name);
                return RedirectToAction("Index");
            }

            var role = await _roleManager.FindByIdAsync(roleModel.Id);

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Редактирование роли - " + role.Name,
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список роли"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Edit", new {role.Id}),
                        Text = "Редактирование роли"
                    }
                }
            };
            return View();
        }

        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var role = await _roleManager.FindByIdAsync(id.Value);
            if (role == null) return HttpNotFound();

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Удалить роль - " + role.Name,
                Breadscrumbs = new List<PageMetaContextUrl>
                {
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Index"),
                        Text = "Список ролей"
                    },
                    new PageMetaContextUrl
                    {
                        Link = Url.Action("Delete", new {role.Id}),
                        Text = "Удалить роли"
                    }
                }
            };

            return View(role);
        }

        //
        // POST: /Roles/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int? id, string deleteUser)
        {
            if (ModelState.IsValid)
            {
                if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                ApplicationIdentityResult result;
                if (deleteUser != null)
                    result = await _roleManager.DeleteAsync(id.Value);
                else
                    result = await _roleManager.DeleteAsync(id.Value);

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }

                return RedirectToAction("Index");
            }

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_roleManager != null)
                {
                    _roleManager.Dispose();
                    _roleManager = null;
                }
            }

            base.Dispose(disposing);
        }
    }
}
