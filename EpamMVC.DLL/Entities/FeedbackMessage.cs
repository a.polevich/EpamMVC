﻿using EpamMVC.DLL.Interfaces;

namespace EpamMVC.DLL.Entities
{
    public class FeedbackMessage : IEntity
    {
//        [Display(Name = "Your name")]
//        [Required, StringLength(120, MinimumLength = 3)]
        public string AuthorName { get; set; }

//        [Required]
//        [DataType(DataType.MultilineText)]
//        [Display(Name = "Message")]
        public string Text { get; set; }
        public int Id { get; set; }
    }
}
