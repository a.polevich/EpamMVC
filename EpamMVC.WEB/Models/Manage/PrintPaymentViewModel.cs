﻿using EpamMVC.DLL.Entities;

namespace EpamMVC.WEB.Models.Manage
{
    public class PrintPaymentViewModel
    {
        public ClientPayment ClientPayment { get; set; }
        public ClientAccount ClientAccount { get; set; }
    }
}
