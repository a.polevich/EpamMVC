﻿using System.Collections.Generic;
using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Models;

namespace EpamMVC.WEB.Models.Manage
{
    public class IndexViewModel
    {
        public bool HasPassword { get; set; }
        public ApplicationIdentityUser User { get; set; }
        public List<ClientAccount> ClientAccounts { get; set; }
        public Dictionary<string, string> SorterLinks { get; set; }
        public string CurrentSortingLabel { get; set; }
    }
}
