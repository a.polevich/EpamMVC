﻿namespace EpamMVC.DLL.Interfaces
{
    public enum OrderDirection
    {
        Ascending,
        Descending
    }
}
