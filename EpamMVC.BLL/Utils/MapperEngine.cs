﻿using System.Collections.Generic;
using System.Reflection;
using AutoMapper;
using EpamMVC.DLL.Models;

namespace EpamMVC.BLL.Utils
{
    public static class MapperEngine
    {
        public static readonly IMapper MapperInstance = new AutoMapper.Mapper(
            new MapperConfiguration(
                cfg => cfg.AddMaps(
                    Assembly.GetExecutingAssembly()
                )
            )
        );

        public static TTarget Map<TTarget>(object entity)
        {
            return MapperInstance.Map<TTarget>(entity);
        }

        public static TTarget Map<TSource, TTarget>(TSource entity)
        {
            return MapperInstance.Map<TSource, TTarget>(entity);
        }

        public static PaginatedList<TTarget> Map<TSource, TTarget>(PaginatedList<TSource> entity)
        {
            return MapperInstance.Map<IList<TSource>, PaginatedList<TTarget>>(entity);
        }

        public static List<TTarget> Map<TSource, TTarget>(List<TSource> entity)
        {
            return MapperInstance.Map<List<TSource>, List<TTarget>>(entity);
        }

//        public static IEnumerable<TTarget> Map<TTarget>(IEnumerable<object> entity)
//            => MapperInstance.Map<IEnumerable<TTarget>>(entity);

//        public static ICollection<TTarget> Map<TTarget>(ICollection<object> entity)
//            => MapperInstance.Map<ICollection<TTarget>>(entity);

//        public static TTarget[] Map<TTarget>(object[] entity)
//            => MapperInstance.Map<TTarget[]>(entity);

//        public static List<TTarget> Map<TTarget>(List<object> entity)
//            => MapperInstance.Map<List<TTarget>>(entity);

//        List<TTarget> listDest = MapperInstance.Map<Source[], List<Destination>>(sources);
//        TTarget[] arrayDest = MapperInstance.Map<Source[], Destination[]>(sources);
    }
}
