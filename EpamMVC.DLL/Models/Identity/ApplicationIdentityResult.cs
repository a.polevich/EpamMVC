﻿using System.Collections.Generic;

namespace EpamMVC.DLL.Models.Identity
{
    public class ApplicationIdentityResult
    {
        public ApplicationIdentityResult(IEnumerable<string> errors, bool succeeded)
        {
            Succeeded = succeeded;
            Errors = errors;
        }

        public IEnumerable<string> Errors { get; }

        public bool Succeeded { get; }
    }
}
