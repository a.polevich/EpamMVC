﻿using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Models;
using EpamMVC.WEB.Models;

namespace EpamMVC.WEB.Areas.Admin.Models.ClientAccountAdmin
{
    public class IndexViewModel
    {
        public PaginatedList<ClientAccount> Accounts { get; set; }
        public Pagination Pagination { get; set; }
    }
}
