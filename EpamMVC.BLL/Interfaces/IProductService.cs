﻿using EpamMVC.DLL.Entities;

namespace EpamMVC.BLL.Interfaces
{
    public interface IProductService : IService<Product>
    {
    }
}
