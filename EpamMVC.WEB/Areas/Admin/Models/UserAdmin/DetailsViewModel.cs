﻿using System.Collections.Generic;
using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Models;

namespace EpamMVC.WEB.Areas.Admin.Models.UserAdmin
{
    public class DetailsViewModel
    {
        public ApplicationIdentityUser User { get; set; }
        public PaginatedList<ClientAccount> Accounts { get; set; }
        public IList<string> Roles { get; set; }
    }
}
