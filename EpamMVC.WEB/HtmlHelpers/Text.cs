using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace EpamMVC.WEB.HtmlHelpers
{
    public static class TextHelper
    {
        public static string Truncate(this HtmlHelper html, string input, int maxLength, bool addEllipsis = true)
        {
            var words = Regex.Replace(input, @"\s+", " ").Split(' ');

            var result = new StringBuilder();

            foreach (var word in words)
            {
                if (result.Length + word.Length > maxLength)
                    break;

                result.Append(' ');
                result.Append(word);
            }

            if (addEllipsis && input.Length > result.Length) result.Append("...");

            return result.ToString();
        }

        public static string StripHtmlTags(this HtmlHelper html, string input)
        {
            return Regex.Replace(input, "<.*?>", string.Empty);
        }

        public static string HtmlToPreview(this HtmlHelper html, string input, int maxLength = 100)
        {
            return Truncate(html, StripHtmlTags(html, input), maxLength);
        }
    }
}
