﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EpamMVC.WEB.Areas.Admin.Models.UserAdmin
{
    public class CreateViewModel
    {
        public bool Locked { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        public IEnumerable<SelectListItem> RolesListAvailable { get; set; }
        public List<string> RolesListSelect { get; set; }
    }
}
