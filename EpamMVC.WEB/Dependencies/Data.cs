﻿using EpamMVC.DLL;
using EpamMVC.DLL.Interfaces;
using Ninject.Modules;

namespace EpamMVC.WEB.Dependencies
{
    public class Data : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>();
        }
    }
}
