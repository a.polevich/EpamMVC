﻿using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Interfaces;

namespace EpamMVC.BLL.Services
{
    public class CallbackService : Service<CallbackRequest>, ICallbackService
    {
        public CallbackService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
