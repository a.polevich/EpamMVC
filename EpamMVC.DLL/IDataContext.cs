﻿using System;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using EpamMVC.DLL.Interfaces;

namespace EpamMVC.DLL
{
    public interface IDataContext : IDisposable
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : class, IEntity;
        void SetAsAdded<TEntity>(TEntity entity) where TEntity : class, IEntity;
        void SetAsModified<TEntity>(TEntity entity) where TEntity : class, IEntity;
        void SetAsDeleted<TEntity>(TEntity entity) where TEntity : class, IEntity;
        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
