﻿using System.Web.Mvc;
using System.Web.Routing;

namespace EpamMVC.WEB
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional},
                new[] {"EpamMVC.WEB.Controllers"}
            );
            routes.AppendTrailingSlash = true;
            routes.LowercaseUrls = true;
        }
    }
}
