﻿import $ from 'jquery';
import diosModal from "../diosModal";

$(function () {
    let $widget = $(".js_manage_account_details");
    if ($widget.length === 0) {
        return
    }

    $widget.on("click", ".js_send_payment", function (event) {
        event.preventDefault();

        let $link = $(this),
            paymentId = $link.data("id");


        if (typeof (paymentId) !== "number" || !(paymentId > 0)) return;

        let $modal = diosModal({
                title: 'Отправить платеж',
                body:
                    `<div class="js_body">Вы уверены что хотите отправить этот платеж?</div>`,
                footer:
                    '<button class="btn btn-primary js_send_agree">Отправить</button>' +
                    '<button class="btn btn-light modal-close js_close">Закрыть</button>',
                vertical_align: true,
                removeOnClose: true
            }),
            strHtmlSuccess = '<div class="alert alert-success" role="alert">Ваша заявка принята!</div>',
            strHtmlError = '<div class="alert alert-danger" role="alert">Произошла ошибка.</div>';

        $modal.on("click", ".js_send_agree", function () {
            $(this).hide();

            $.ajax({
                method: "post",
                url: $link.attr("href"),
                data: {
                    paymentId: paymentId
                },
                success: function () {
                    $(".js_body", $modal).html(strHtmlSuccess);
                },
                error: function () {
                    console.log('pass6');
                    $(".js_body", $modal).html(strHtmlError);
                }
            });

            $(".js_close", $modal).on("click", function () {
                window.location.reload(true);
            });
        });

        return false;
    });


});
