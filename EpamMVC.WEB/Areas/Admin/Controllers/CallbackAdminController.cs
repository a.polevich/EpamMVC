﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Interfaces;
using EpamMVC.WEB.Areas.Admin.Models.CallbackAdmin;
using EpamMVC.WEB.Models;
using EpamMVC.WEB.Utils;

namespace EpamMVC.WEB.Areas.Admin.Controllers
{
    [Authorize(Roles = "Администраторы")]
    public class CallbackAdminController : Controller
    {
        private readonly ICallbackService _callbackService;
        private readonly int _pageSize = 20;

        public CallbackAdminController(ICallbackService callbackService)
        {
            _callbackService = callbackService;
        }

        public async Task<ActionResult> Index(int pageIndex = 1)
        {
            var items = await _callbackService.GetAsync(
                pageIndex,
                _pageSize,
                x => x.Id,
                OrderDirection.Descending
            );

            ViewBag.pageMeta = new PageMetaContext
            {
                MetaTitle = "Заявки на звонок"
            };

            return View(
                new IndexViewModel
                {
                    Requests = items,
                    Pagination = new Pagination
                    {
                        UrlTemplate = Url.Action("Index", new {pageIndex = "PAGE_INDEX"}),
                        PageSize = _pageSize,
                        CurrentPageNumber = pageIndex,
                        TotalItems = items.TotalCount
                    }
                }
            );
        }

        [HttpPost]
        public async Task<HttpStatusCodeResult> Mark(int id, bool calledState)
        {
            var callbackRequest = await _callbackService.GetByIdAsync(id);
            if (callbackRequest != null)
            {
                callbackRequest.Called = calledState;
                await _callbackService.UpdateAsync(callbackRequest);

                return new HttpStatusCodeResult(HttpStatusCode.Accepted);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public async Task<HttpStatusCodeResult> Delete(int id)
        {
            var callbackRequest = await _callbackService.GetByIdAsync(id);
            if (callbackRequest != null)
            {
                await _callbackService.DeleteAsync(callbackRequest);

                return new HttpStatusCodeResult(HttpStatusCode.Accepted);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    }
}
