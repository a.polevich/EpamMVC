﻿using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Interfaces;

namespace EpamMVC.BLL.Services
{
    public class ExceptionLogService : Service<ExceptionLog>, IExceptionLogService
    {
        public ExceptionLogService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
