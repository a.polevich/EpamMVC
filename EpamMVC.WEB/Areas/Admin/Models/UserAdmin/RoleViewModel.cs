﻿using System.ComponentModel.DataAnnotations;

namespace EpamMVC.WEB.Areas.Admin.Models.UserAdmin
{
    public class RoleViewModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Необходимо указать имя роли")]
        [Display(Name = "Роль")]
        public string Name { get; set; }
    }
}
