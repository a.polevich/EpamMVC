﻿import $ from 'jquery';
import diosModal from "../diosModal";

$(function () {
    let $widget = $(".js_manage_index");
    if ($widget.length === 0) {
        return
    }

    $widget.on("click", ".js_lock_account", function (event) {
        event.preventDefault();

        let $link = $(this),
            accountId = $link.data("id");


        if (typeof accountId !== "number" || !(accountId > 0)) return;

        let $modal = diosModal({
                title: 'Заблокировать счет',
                body:
                    `<div class="js_body">
Вы уверены что хотите заблокировать этот счет/карту?<br/>
Разблокировать счет вы сможете только через заявку, обработку которой может занять время.
                    </div>`,
                footer:
                    '<button class="btn btn-danger js_lock_agree">Заблокировать</button>' +
                    '<button class="btn btn-light modal-close js_close">Закрыть</button>',
                vertical_align: true,
                removeOnClose: true
            }),
            strHtmlSuccess = '<div class="alert alert-success" role="alert">Счет заблокирован!</div>',
            strHtmlError = '<div class="alert alert-danger" role="alert">Произошла ошибка.</div>';

        $modal.on("click", ".js_lock_agree", function () {
            $(this).hide();

            $.ajax({
                method: "post",
                url: $link.attr("href"),
                data: {
                    accountId: accountId
                },
                success: function () {
                    $(".js_body", $modal).html(strHtmlSuccess);

                    $(".js_close", $modal).on("click", function () {
                        window.location.reload(true);
                    });
                },
                error: function () {
                    $(".js_body", $modal).html(strHtmlError);
                }
            });
        });
    });

    $widget.on("click", ".js_unlock_account", function (event) {
        event.preventDefault();

        let $link = $(this),
            accountId = $link.data("id");

        if (typeof accountId !== "number" || !(accountId > 0)) return;

        let $modal = diosModal({
                title: 'Разблокировать счет',
                body:
                    `<div class="js_body">
Вы уверены что хотите разблокировать этот счет/карту?<br/>
Процедура разблокировки может занять время.
                    </div>`,
                footer:
                    '<button class="btn btn-danger js_unlock_agree">Разблокировать</button>' +
                    '<button class="btn btn-light modal-close js_close">Закрыть</button>',
                vertical_align: true,
                removeOnClose: true
            }),
            strHtmlSuccess = '<div class="alert alert-success" role="alert">Ваша заявка принята и в ближайшее время мы свяжемся с вами!</div>',
            strHtmlError = '<div class="alert alert-danger" role="alert">Произошла ошибка.</div>';

        $modal.on("click", ".js_unlock_agree", function () {
            $(this).hide();

            $.ajax({
                method: "post",
                url: $link.attr("href"),
                data: {
                    accountId: accountId
                },
                success: function () {
                    $(".js_body", $modal).html(strHtmlSuccess);
                    $(".js_close", $modal).on("click", function () {
                        window.location.reload(true);
                    });
                },
                error: function () {
                    $(".js_body", $modal).html(strHtmlError);
                }
            });
        });
    });

    $widget.on("click", ".js_add_funds", function (event) {
        event.preventDefault();

        let $link = $(this),
            accountId = $link.data("id");

        if (typeof accountId !== "number" || !(accountId > 0)) return;

        let $modal = diosModal({
                title: 'Пополнение счета',
                body:
                    `<div class="js_body">
<p>На какую сумму вы хотили бы пополнить счет?</p>
<input type="number" class="form-control js_amount" autocomplete="off"/>
                    </div>`,
                footer:
                    '<button class="btn btn-danger js_add_funds_agree">Пополнить</button>' +
                    '<button class="btn btn-light modal-close js_close">Закрыть</button>',
                vertical_align: true,
                removeOnClose: true
            }),
            strHtmlSuccess = '<div class="alert alert-success" role="alert">Вы успешно пополнили баланс карты!</div>',
            strHtmlError = '<div class="alert alert-danger" role="alert">Произошла ошибка.</div>';

        $modal.on("click", ".js_add_funds_agree", function () {
            let $input = $(".js_amount", $modal);
            let amount = parseInt($input.val());

            if (!(amount > 0)) {
                $input.trigger("focus");
                return;
            }

            $(this).hide();

            $.ajax({
                method: "post",
                url: $link.attr("href"),
                data: {
                    accountId: accountId,
                    amount: amount
                },
                success: function () {
                    $(".js_body", $modal).html(strHtmlSuccess);
                    $(".js_close", $modal).on("click", function () {
                        window.location.reload(true);
                    });
                },
                error: function () {
                    $(".js_body", $modal).html(strHtmlError);
                }
            });
        });
    });

    $widget.on("click", ".js_create_account", function (event) {
        event.preventDefault();
        let $link = $(this);

        let $modal = diosModal({
                title: 'Зарегистрировать новую карту?',
                body:
                    `<div class="js_body">
                        <p>Вы уверены, что хотите зарегистрировать новую карту?</p>
                    </div>`,
                footer:
                    '<button class="btn btn-success js_create_account_agree">Зарегистрировать</button>' +
                    '<button class="btn btn-light modal-close js_close">Закрыть</button>',
                vertical_align: true,
                removeOnClose: true
            }),
            strHtmlSuccess = '<div class="alert alert-success" role="alert">Карта успешно зарегистрирована!</div>',
            strHtmlError = '<div class="alert alert-danger" role="alert">Произошла ошибка.</div>';


        $modal.on("click", ".js_create_account_agree", function () {
            $(this).hide();

            $.ajax({
                method: "post",
                url: $link.attr("href"),
                data: {},
                success: function () {
                    $(".js_body", $modal).html(strHtmlSuccess);
                    $(".js_close", $modal).on("click", function () {
                        window.location.reload(true);
                    });
                },
                error: function () {
                    $(".js_body", $modal).html(strHtmlError);
                }
            });
        });
    });

    $widget.on("click", ".js_create_payment", function (event) {

        event.preventDefault();

        let $link = $(this),
            accountId = $link.data("id");

        if (typeof accountId !== "number" || !(accountId > 0)) return;

        let $modal = diosModal({
                title: 'Добавить платеж',
                body:
                    `<div class="js_body">
                        <p>На какую сумму вы хотили добавить платеж?</p>
                        <input type="number" class="form-control js_amount" autocomplete="off"/>
                    </div>`,
                footer:
                    '<button class="btn btn-success js_create_payment_agree">Добавить</button>' +
                    '<button class="btn btn-light modal-close js_close">Закрыть</button>',
                vertical_align: true,
                removeOnClose: true
            }),
            strHtmlSuccess = '<div class="alert alert-success" role="alert">Платеж успешно сделан!</div>',
            strHtmlNotEnough = '<div class="alert alert-warning" role="alert">На счету не достаточно средств</div>',
            strHtmlError = '<div class="alert alert-danger" role="alert">Произошла ошибка.</div>';

        $modal.on("click", ".js_create_payment_agree", function () {
            let $input = $(".js_amount", $modal);
            let amount = parseInt($input.val());

            if (!(amount > 0)) {
                $input.trigger("focus");
                return;
            }

            $(this).hide();

            $.ajax({
                method: "post",
                url: $link.attr("href"),
                data: {
                    accountId: accountId,
                    amount: amount
                },
                success: function (data) {
                    if (data["result"] == "not_enough") {
                        $(".js_body", $modal).html(strHtmlNotEnough);
                    } else if (data["result"] == "success") {
                        $(".js_body", $modal).html(strHtmlSuccess);
                        $(".js_close", $modal).on("click", function () {
                            window.location.reload(true);
                        });
                    }
                },
                error: function () {
                    $(".js_body", $modal).html(strHtmlError);
                }
            });
        });
    });
});
