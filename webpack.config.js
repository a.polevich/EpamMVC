const mode = process.env.NODE_ENV || 'development';
const isDevelopment = mode === 'development';

const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const AutoprefixerPlugin = require('autoprefixer');
const cssnano = require('cssnano');

const loaders = {
    babel: {
        loader: 'babel-loader',
        options: {
            presets: ['@babel/preset-env'],
            cacheDirectory: true
        }
    },
    css: {
        loader: "css-loader",

        options: {}
    },
    postCss: {
        loader: 'postcss-loader',
        options: {
            plugins: [AutoprefixerPlugin, cssnano],
            sourceMap: true,
        }
    },
    less: {
        loader: 'less-loader',
        options: {
            strictMath: true,
            noIeCompat: true,
            sourceMap: true
        }
    },
    sass: {
        loader: 'sass-loader',
        options: {
            sourceMap: true
        }
    },
    images: {
        loader: 'url-loader',
        options: {
            limit: 8192,
            outputPath: 'images'
        }
    },
    fonts: {
        loader: 'url-loader',
        options: {
            limit: 8192,
            outputPath: 'fonts'
        }
    }
};


const jsModule = {
    test: /\.js$/,
    exclude: /(node_modules)/,
    use: [
        loaders.babel
    ]
};

const scssModule = {
    test: /\.s?[ac]ss$/,
    use: [
        MiniCssExtractPlugin.loader,
        loaders.css,
        loaders.postCss,
        loaders.sass
    ],
};

const lessModule = {
    test: /\.less$/,
    exclude: /(tinymce)/,
    use: [
        MiniCssExtractPlugin.loader,
        loaders.css,
        loaders.postCss,
        loaders.less
    ]
};

const cssModule = {
    test: /\.css$/,
    exclude: /(tinymce)/,
    use: [
        MiniCssExtractPlugin.loader,
        loaders.css,
        loaders.postCss
    ]
};

const imagesModule = {
    test: /\.(png|jpeg|jpg|gif|svg)$/i,
    use: [
        loaders.images
    ]
};

const fontsModule = {
    test: /\.(woff|woff2|eot|ttf)$/i,
    use: [
        loaders.fonts
    ]
};

const tinymceModule = {
    test: /tinymce/,
    exclude: /\.js$/,
    loader: 'file-loader',
    options: {
        name: '[path][name].[ext]',
        context: "node_modules"
    },
};


module.exports = {
    mode: mode,
    plugins: [
        new MiniCssExtractPlugin({filename: '[name].css', chunkFilename: '[id].css'}),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ],
    entry: {
        bundle: './EpamMVC.WEB/Content/src/bundle.js',
        admin_bundle: './EpamMVC.WEB/Content/src/admin_bundle.js',
    },
    output: {
        path: path.resolve(__dirname, 'EpamMVC.WEB/Content/dist'),
        filename: '[name].js'
    },

    devtool: isDevelopment && "source-map",
    module: {
        rules: [
            tinymceModule,
            jsModule,
            cssModule,
            lessModule,
            fontsModule,
            imagesModule
        ]
    },
    watchOptions: {
        ignored: /node_modules/
    }
};
