﻿using AutoMapper;
using EpamMVC.DLL.Entities;
using EpamMVC.WEB.Models.Callback;

namespace EpamMVC.WEB.Utils.Mapper
{
    public class CallbackRequestProfile : Profile
    {
        public CallbackRequestProfile()
        {
            CreateMap<CallbackRequestAjaxSubmit, CallbackRequest>();
        }
    }
}
