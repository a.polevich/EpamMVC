﻿using System.Collections.Generic;

namespace EpamMVC.DLL.Models.Identity
{
    public class ApplicationRole
    {
        public ApplicationRole()
        {
            Users = new List<ApplicationUserRole>();
        }

        public int Id { get; set; }

        public virtual ICollection<ApplicationUserRole> Users { get; }

        public string Name { get; set; }
    }
}
