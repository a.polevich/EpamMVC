﻿using System.Web.Mvc;

namespace EpamMVC.WEB
{
    public static class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

//            filters.Add(new ExceptionFilterAttribute());
        }
    }
}
