using EpamMVC.BLL.Interfaces;
using EpamMVC.BLL.Services;
using Ninject.Modules;

namespace EpamMVC.WEB.Dependencies
{
    public class Business : NinjectModule
    {
        public override void Load()
        {
            Bind<IExceptionLogService>().To<ExceptionLogService>();
            Bind<ICallbackService>().To<CallbackService>();
            Bind<IClientAccountService>().To<ClientAccountService>();
            Bind<IClientPaymentService>().To<ClientPaymentService>();
        }
    }
}
