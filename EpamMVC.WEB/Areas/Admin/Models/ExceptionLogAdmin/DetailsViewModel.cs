﻿using EpamMVC.DLL.Entities;

namespace EpamMVC.WEB.Areas.Admin.Models.ExceptionLogAdmin
{
    public class DetailsViewModel
    {
        public ExceptionLog LogRecord { get; set; }
    }
}
