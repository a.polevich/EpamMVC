using System;

namespace EpamMVC.WEB.Models
{
    public class Pagination
    {
//        public string Controller { get; set; }
//        public string Action { get; set; }
        public string UrlTemplate { get; set; }
        public string PageIndexPlaceHolder { get; set; } = "PAGE_INDEX";

        public int CurrentPageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }

        public int TotalPages => (int) Math.Ceiling((decimal) TotalItems / PageSize);
    }
}
