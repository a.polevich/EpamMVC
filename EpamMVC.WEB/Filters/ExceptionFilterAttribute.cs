﻿using System;
using System.Web.Mvc;
using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Entities;
using EpamMVC.WEB.Models;

namespace EpamMVC.WEB.Filters
{
    public class ExceptionFilterAttribute : FilterAttribute, IExceptionFilter
    {
        private readonly IExceptionLogService _exceptionLogService;

        public ExceptionFilterAttribute(IExceptionLogService exceptionLogService)
        {
            _exceptionLogService = exceptionLogService;
        }

        public void OnException(ExceptionContext filterContext)
        {
            var exceptionDetail = new ExceptionLog
            {
                Exception = filterContext.Exception.Message,
                StackTrace = filterContext.Exception.StackTrace,
                ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                ActionName = filterContext.RouteData.Values["action"].ToString(),
                DateTime = DateTime.Now
            };

            _exceptionLogService.Add(exceptionDetail);

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.StatusCode = 503;
            filterContext.HttpContext.Response.StatusDescription = "Service Unavailable";
//            filterContext.Result = new HttpStatusCodeResult(503);
            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(new EmptyModelMetadataProvider())
                {
                    Model = new ErrorViewModel()
                }
            };
        }
    }
}
