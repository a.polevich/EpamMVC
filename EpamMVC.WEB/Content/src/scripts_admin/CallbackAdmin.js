﻿import $ from 'jquery';
import diosModal from "../diosModal";

$(function () {

    function showError() {
        diosModal({
            title: 'Ошибка',
            body: `<div class="alert alert-danger" role="alert">Произошла ошибка. Сообщение не отправлено.</div>`,
            footer: '<button class="btn btn-light modal-close">Закрыть</button>',
            vertical_align: true,
            removeOnClose: true
        });
    }

    let $widget = $(".js_callbackTableWidget");
    if ($widget.length === 0) {
        return
    }

    $widget.on('click', "a.js_callback_mark", function () {

        let $link = $(this),
            $row = $link.closest(".js_callback_row", $widget),
            bCalled = $link.data("called");

        if (bCalled !== true) {
            bCalled = false;
        }

        $.ajax({
            method: "post",
            url: $link.attr("href"),
            data: {
                calledState: !bCalled
            },
            success: function () {
                $row.toggleClass("table-success");
            },
            error: showError
        });
        return false;

    });

    $widget.on('click', "a.js_callback_delete", function () {
        let $link = $(this),
            $row = $link.closest(".js_callback_row", $widget);

        $.ajax({
            method: "post",
            url: $(this).attr("href"),
            success: function () {
                $row.remove();
            },
            error: showError

        });

        return false;
    });
});
