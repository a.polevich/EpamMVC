﻿using EpamMVC.BLL.Interfaces;
using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Interfaces;

namespace EpamMVC.BLL.Services
{
    public class ClientPaymentService : Service<ClientPayment>, IClientPaymentService
    {
        public ClientPaymentService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
