﻿using EpamMVC.DLL.Entities;
using EpamMVC.DLL.Models;
using EpamMVC.WEB.Models;

namespace EpamMVC.WEB.Areas.Admin.Models.CallbackAdmin
{
    public class IndexViewModel
    {
        public PaginatedList<CallbackRequest> Requests { get; set; }
        public Pagination Pagination { get; set; }
    }
}
