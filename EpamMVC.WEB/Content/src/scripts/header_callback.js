﻿import $ from 'jquery';
import diosModal from '../diosModal';
import Inputmask from "inputmask/dist/inputmask/jquery.inputmask";

$(function () {

    $(".js_callback").on("click", function () {

        let ajaxUrl = $(this).data("url");

        if (typeof (ajaxUrl) !== "string" && ajaxUrl === "") {
            return;
        }

        let $modal = diosModal({
                class: "modal_callback",
                title: 'Подобрать тур',
                body: `
<form class="callback" action="/">
    <input type="text" name="name" value="" class="name form-control" placeholder="Ваше имя" autocomplete="off"/>
    <input type="text" name="phone" value="" class="phone form-control" placeholder="Ваш номер телефона" autocomplete="off"/>
    <p>* Нажимая кнопку «Отправить», я соглашаюсь на обработку <a href="/policy/polzovatelskoe-soglashenie.php">персональных данных</a>.</p>
    <p>Этот сайт защищен reCAPTCHA и применяются <a href="https://policies.google.com/privacy">Политика конфиденциальности</a> и <a href="https://policies.google.com/terms">Условия обслуживания</a> Google.</p>
</form>
`, footer:
                '<button class="btn btn-light modal-close">Закрыть</button>' +
                '<button class="btn btn-primary js_submit">Отправить</button>'
                ,
                vertical_align: true,
                removeOnClose: true
            }),
            strHtmlSuccess = '<div class="alert alert-success" role="alert">Ваша заявка принята и в ближайшее время мы свяжемся с вами!</div>',
            strHtmlError = '<div class="alert alert-danger" role="alert">Произошла ошибка. Сообщение не отправлено.</div>',
            $inputPhone = $("input.phone", $modal),
            $inputName = $("input.name", $modal);

        $inputPhone.inputmask({
            mask: '38 999 999-99-99'
        });

        let $submitBtn = $(".js_submit", $modal).on('click', function (event) {
            if ($inputPhone.val().length <= 3) {
                $inputPhone.trigger("focus");
                return false;
            }

            $submitBtn.hide();
            // todo add blocking styles
            $.ajax({
                method: "post",
                url: ajaxUrl,
                dataType: "json",
                data: {
                    PHONE: $inputPhone.val(),
                    NAME: $inputName.val(),
                    URL: window.location.href
                },
                success: function (data) {
                    if (data.result === 'success') {
                        $("form", $modal).replaceWith(strHtmlSuccess);
                    } else {
                        $("form", $modal).replaceWith(strHtmlError);
                    }

                },
                error: function () {
                    $("form", $modal).replaceWith(strHtmlError);
                }
            });
            return false;
        });

    });

});

