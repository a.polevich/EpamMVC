﻿using EpamMVC.DLL.Models;
using EpamMVC.WEB.Models;

namespace EpamMVC.WEB.Areas.Admin.Models.UserAdmin
{
    public class IndexViewModel
    {
        public PaginatedList<ApplicationIdentityUser> Users { get; set; }
        public Pagination Pagination { get; set; }
    }
}
