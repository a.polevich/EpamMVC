﻿import $ from 'jquery';

$(function () {
    "use strict";

    let $window = $(window),
        $button = $('<div id="dios_scrollup"></div>').appendTo('body');

    $window.on('scroll', function () {
        if ($window.scrollTop() > 300) {
            $button.addClass('show');
        } else {
            $button.removeClass('show');
        }
    });

    $button.on('click', function () {
        $('body,html').animate({ scrollTop: 0 }, 400);
        return false;
    });
});
