using System;
using System.Threading.Tasks;

namespace EpamMVC.DLL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> Repository<TEntity>() where TEntity : class, IEntity;

        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
