using EpamMVC.DLL.Interfaces;
using EpamMVC.DLL.Models;

namespace EpamMVC.DLL.Entities
{
    public class ClientAccount : IEntity
    {
        public bool Locked { get; set; }
        public bool UnlockRequest { get; set; }
        public decimal Balance { get; set; }
        public int UserId { get; set; }
        public ApplicationIdentityUser User { get; set; }
        public int Id { get; set; }
    }
}
